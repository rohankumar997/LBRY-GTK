#####################################################################
#                                                                   #
#  THIS IS A SOURCE CODE FILE FROM A PROGRAM TO INTERACT WITH THE   #
# LBRY PROTOCOL ( lbry.com ). IT WILL USE THE LBRY SDK ( lbrynet )  #
# FROM THEIR REPOSITORY ( https://github.com/lbryio/lbry-sdk )      #
# WHICH I GONNA PRESENT TO YOU AS A BINARY. SINCE I DID NOT DEVELOP #
# IT AND I'M LAZY TO INTEGRATE IN A MORE SMART WAY. THE SOURCE CODE #
# OF THE SDK IS AVAILABLE IN THE REPOSITORY MENTIONED ABOVE.        #
#                                                                   #
#      ALL THE CODE IN THIS REPOSITORY INCLUDING THIS FILE IS       #
# (C) J.Y.Amihud and Other Contributors 2021. EXCEPT THE LBRY SDK.  #
# YOU CAN USE THIS FILE AND ANY OTHER FILE IN THIS REPOSITORY UNDER #
# THE TERMS OF GNU GENERAL PUBLIC LICENSE VERSION 3 OR ANY LATER    #
# VERSION. TO FIND THE FULL TEXT OF THE LICENSE GO TO THE GNU.ORG   #
# WEBSITE AT ( https://www.gnu.org/licenses/gpl-3.0.html ).         #
#                                                                   #
# THE LBRY SDK IS UNFORTUNATELY UNDER THE MIT LICENSE. IF YOU ARE   #
# NOT INTENDING TO USE MY CODE AND JUST THE SDK. YOU CAN FIND IT ON #
# THEIR OFFICIAL REPOSITORY ABOVE. THEIR LICENSE CHOICE DOES NOT    #
# SPREAD ONTO THIS PROJECT. DON'T GET A FALSE ASSUMPTION THAT SINCE #
# THEY USE A PUSH-OVER LICENSE, I GONNA DO THE SAME. I'M NOT.       #
#                                                                   #
# THE LICENSE CHOSEN FOR THIS PROJECT WILL PROTECT THE 4 ESSENTIAL  #
# FREEDOMS OF THE USER FURTHER, BY NOT ALLOWING ANY WHO TO CHANGE   #
# THE LICENSE AT WILL. SO NO PROPRIETARY SOFTWARE DEVELOPER COULD   #
# TAKE THIS CODE AND MAKE THEIR USER-SUBJUGATING SOFTWARE FROM IT.  #
#                                                                   #
#####################################################################

# This file will perform a simple search on the LBRY network.

import requests

from flbry import parse, error


def simple(
    text="",
    channel="",
    any_tags=[],
    all_tags=[],
    not_tags=[],
    any_languages=[],
    all_languages=[],
    not_languages=[],
    order_by=[],
    timestamp="-1",
    media_types="",
    channel_ids=[],
    not_channel_ids=[],
    claim_ids=[],
    claim_type="",
    stream_types=[],
    page_size=5,
    page=1,
    server="http://localhost:5279",
):
    json = {
        "method": "claim_search",
        "params": {
            "page_size": page_size,
            "page": page,
            "no_totals": True,
        },
    }

    converts = {
        "channel_ids": channel_ids,
        "not_channel_ids": not_channel_ids,
        "claim_ids": claim_ids,
    }
    for convert in converts:
        item = converts[convert]
        for index in range(len(item)):
            if "#" in item[index]:
                item[index] = item[index].split("#", 1)[1]
        if item != []:
            json["params"][convert] = item

    if text != "":
        json["params"]["text"] = text

    if order_by != []:
        json["params"]["order_by"] = order_by

    if timestamp != "-1":
        json["params"]["timestamp"] = timestamp

    if channel != "":
        json["params"]["channel"] = channel

    if media_types != "":
        json["params"]["media_types"] = media_types

    if claim_type != "":
        json["params"]["claim_type"] = claim_type

    if stream_types != []:
        json["params"]["stream_types"] = stream_types

    if any_tags != []:
        json["params"]["any_tags"] = any_tags

    if all_tags != []:
        json["params"]["all_tags"] = all_tags

    if not_tags != []:
        json["params"]["not_tags"] = not_tags

    if any_languages != []:
        json["params"]["any_languages"] = any_languages

    if all_languages != []:
        json["params"]["all_languages"] = all_languages

    if not_languages != []:
        json["params"]["not_languages"] = not_languages

    try:
        json_data = {"error": {}}
        json_data = requests.post(server, json=json).json()
        json_data = json_data["result"]
    except Exception as e:
        return error.error(e, json_data["error"])

    return parse.publications(json_data, server=server)
