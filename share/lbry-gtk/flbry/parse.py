################################################################################
# LBRY-GTK Internals                                                           #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import traceback
from flbry import url, error


def single_notification(i, server="http://localhost:5279"):
    title = ""
    ftype = "claim"
    timestamp = 0
    pub_url = ""
    bywho = ""
    image_url = ""
    claim_id = ""

    try:
        title = i["notification_parameters"]["dynamic"]["claim_title"]
    except:
        pass

    try:
        ftype = i["notification_rule"]
    except:
        pass

    try:
        pub_url = i["notification_parameters"]["device"]["target"]
    except:
        pass

    try:
        if ftype == "new_content":
            bywho_helper = i["notification_parameters"]["dynamic"][
                "channel_url"
            ]
            bywho = bywho_helper[7 : bywho_helper.find("#")]
        elif ftype == "comment":
            bywho_helper = i["notification_parameters"]["dynamic"][
                "comment_author"
            ]
            bywho = bywho_helper[7 : bywho_helper.find("#")]
        elif ftype == "comment-reply":
            bywho = i["notification_parameters"]["device"]["title"].split(" ")[
                0
            ]
    except:
        pass

    try:
        image_url = i["notification_parameters"]["dynamic"][
            "comment_author_thumbnail"
        ]
    except:
        pass

    try:
        timestamp = i["created_at"]
    except:
        pass

    if image_url == "":
        try:
            image_url = i["notification_parameters"]["device"]["image_url"]
        except:
            pass

        try:
            image_url = i["notification_parameters"]["dynamic"][
                "claim_thumbnail"
            ]
        except:
            pass

    try:
        claim_id = (
            i["notification_parameters"]["device"]["link"]
            .split(":")[2]
            .split("?")[0]
        )
    except:
        pass
    if not "?" in pub_url:
        return [bywho, title, timestamp, ftype, pub_url, image_url, claim_id]


def single_publication(i, server="http://localhost:5279"):
    title = ""
    ftype = "claim"
    timestamp = 0
    pub_url = ""
    bywho = ""
    image_url = ""
    claim_id = ""

    try:
        try:
            title = i["value"]["title"]
        except:
            title = i["name"]

        try:
            claim_id = i["claim_id"]
        except:
            pass

        try:
            timestamp = i["timestamp"]
        except:
            pass

        try:
            ftype = i["value"]["stream_type"]
        except:
            ftype = i["value_type"]
        try:
            pub_url = i["permanent_url"]
        except:
            pub_url = i["canonical_url"]

        try:
            bywho = i["signing_channel"]["name"]
        except:
            bywho = i["name"]

        if ftype != "repost":
            try:
                image_url = i["value"]["thumbnail"]["url"]
            except:
                pass
        else:
            try:
                image_url = url.get([pub_url], server=server)[0]["value"][
                    "thumbnail"
                ]["url"]
            except:
                pass

    except:
        pass

    return [bywho, title, timestamp, ftype, pub_url, image_url, claim_id]


def single_comment(i, server="http://localhost:5279"):
    comment = ""
    support = 0
    replies = 0
    bywho = ""
    reply_comment_id = ""
    channel_url = ""
    image_url = ""
    amount = 0
    number = 0

    try:
        try:
            comment = i["comment"]
        except:
            pass

        try:
            support = i["support_amount"]
        except:
            pass

        try:
            replies = i["replies"]
        except:
            pass

        try:
            reply_comment_id = i["comment_id"]
        except:
            pass

        try:
            channel_url = i["channel_url"]
        except:
            pass

        try:
            timestamp = i["timestamp"]
        except:
            pass

        try:
            if "thumbnail" in i["resolved"]["value"]:
                image_url = i["resolved"]["value"]["thumbnail"]["url"]
            bywho = i["resolved"]["canonical_url"][7:]
            amount = i["resolved"]["meta"]["effective_amount"]
            number = i["resolved"]["meta"]["claims_in_channel"]
        except:
            try:
                bywho = i["channel_name"]
            except:
                pass

    except:
        pass

    return [
        replies,
        support,
        bywho,
        comment,
        reply_comment_id,
        channel_url,
        image_url,
        timestamp,
        amount,
        number,
    ]


def single_channel(i, server="http://localhost:5279"):
    title = ""
    ftype = "claim"
    pub_url = ""
    bywho = ""
    image_url = ""
    claim_id = ""
    try:
        try:
            bywho = i["name"]
        except:
            pass

        try:
            pub_url = i["permanent_url"]
        except:
            pass

        try:
            image_url = i["value"]["thumbnail"]["url"]
        except:
            pass

        try:
            ftype = i["value_type"]
        except:
            pass

        try:
            title = i["value"]["title"]
        except:
            pass

        try:
            claim_id = i["claim_id"]
        except:
            pass

    except:
        pass

    return [bywho, title, ftype, pub_url, image_url, claim_id]


def single_balance(i, server="http://localhost:5279"):
    confirm = ""
    amount = ""
    tip = False
    publication = ""
    timestamp = 0
    bywho = ""
    pub_url = ""
    image_url = ""
    ftype = ""
    claim_id = ""

    try:
        confirm = i["confirmations"]
    except:
        pass

    try:
        timestamp = i["timestamp"]
    except:
        pass

    try:
        amount = float(i["value"])
    except:
        pass

    try:
        tip = i["support_info"][0]["is_tip"]
    except:
        pass

    if i["support_info"] != []:
        info = i["support_info"][0]
    elif i["update_info"] != []:
        info = i["update_info"][0]
    elif i["abandon_info"] != []:
        info = i["abandon_info"][0]
    elif i["claim_info"] != []:
        info = i["claim_info"][0]
    elif i["purchase_info"] != []:
        info = i["purchase_info"][0]

    try:
        publication = info["claim_name"]
        claim_id = info["claim_id"]
        amount = float(info["amount"])
    except:
        pass

    try:
        if publication != "":
            json_data = url.get([claim_id], server=server)[0]

            if isinstance(json_data, str):
                return json_data

            try:
                pub_url = json_data["canonical_url"]
            except:
                pass
            try:
                image_url = json_data["value"]["thumbnail"]["url"]
            except:
                pass

            try:
                bywho = json_data["signing_channel"]["name"]
            except:
                bywho = json_data["name"]

            try:
                ftype = json_data["value"]["stream_type"]
            except:
                ftype = json_data["value_type"]
    except:
        pass

    return [
        confirm,
        amount,
        tip,
        bywho,
        publication,
        timestamp,
        ftype,
        pub_url,
        image_url,
        claim_id,
    ]


def single_file(i, server="http://localhost:5279"):
    title = ""
    ftype = "claim"
    timestamp = 0
    bywho = ""
    pub_url = ""
    image_url = ""
    claim_id = ""

    try:
        try:
            title = i["metadata"]["title"]
        except:
            title = i["claim_name"]
        try:
            bywho = i["channel_name"]
        except:
            pass

        try:
            ftype = i["metadata"]["stream_type"]
        except:
            pass

        pub_url = i["claim_name"] + "#" + i["claim_id"]

        try:
            image_url = i["metadata"]["thumbnail"]["url"]
        except:
            pass

        try:
            timestamp = i["added_on"]
        except:
            pass

        try:
            claim_id = i["claim_id"]
        except:
            pass

    except:
        pass
    return [bywho, title, timestamp, ftype, pub_url, image_url, claim_id]


def single_wallet(i, server="http://localhost:5279"):
    id = ""
    name = ""
    try:
        id = i["id"]
    except:
        pass

    try:
        name = i["name"]
    except:
        pass

    return [id, name]


parsers = {
    "notification": single_notification,
    "publication": single_publication,
    "comment": single_comment,
    "channel": single_channel,
    "balance": single_balance,
    "wallet": single_wallet,
    "file": single_file,
}


def notifications(json_data, server="http://localhost:5279"):
    return parse(json_data, "notification", server=server, no_items=True)


def publications(json_data, server="http://localhost:5279"):
    return parse(json_data, "publication", server=server)


def comments(json_data, server="http://localhost:5279"):
    try:
        urls = []
        for item in json_data["items"]:
            urls.append(item["channel_url"])
        resolved = url.get(urls, server=server)
        for index in range(len(json_data["items"])):
            json_data["items"][index]["resolved"] = resolved[index]
    except:
        pass
    return parse(json_data, "comment", server=server)


def channels(json_data, server="http://localhost:5279"):
    return parse(json_data, "channel", server=server)


def wallets(json_data, server="http://localhost:5279"):
    return parse(json_data, "wallet", server=server)


def balances(json_data, server="http://localhost:5279"):
    return parse(json_data, "balance", server=server)


def files(json_data, server="http://localhost:5279"):
    return parse(json_data, "file", server=server)


def parse(
    json_data, parse_type, server="http://localhost:5279", no_items=False
):
    # Now we want to parse the json

    try:
        if no_items:
            items = json_data
        else:
            items = json_data["items"]
        data = []

        # List what we found
        for n, i in enumerate(items):
            single_data = parsers[parse_type](i, server=server)
            if isinstance(single_data, str):
                return single_data
            if single_data:
                data.append(single_data)

        return data

    # Error messages
    except Exception as e:
        if "error" in json_data:
            return error.error(e, json_data["error"])
        return error.error(e, json_data)
