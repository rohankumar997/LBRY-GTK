################################################################################
# LBRY-GTK Internals                                                           #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

# This file will perform a simple search on the LBRY network.

import json, requests

from flbry import error


def create(
    claim_id,
    amount,
    tip=False,
    channel_name="",
    channel_id="",
    server="http://localhost:5279",
):

    new_ammount = ("%0.15f" % amount).rstrip("0")

    if new_ammount.endswith("."):
        new_ammount += "0"

    json = {
        "method": "support_create",
        "params": {
            "claim_id": claim_id,
            "amount": new_ammount,
            "tip": tip,
        },
    }

    if channel_id != "":
        json["params"]["channel_id"] = channel_id

    if channel_name != "":
        json["params"]["channel_name"] = channel_name

    try:
        json_data = {"error": {}}
        json_data = requests.post(server, json=json).json()
        json_data = json_data["result"]
    except Exception as e:
        return error.error(e, json_data["error"])

    return json_data
