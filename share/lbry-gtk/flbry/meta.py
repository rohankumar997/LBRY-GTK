################################################################################
# LBRY-GTK Internals                                                           #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import json, requests, queue, threading

from flbry import url, channel, parse, settings, error


def get_type(meta_server, auth_token, notification_type, type_queue):
    try:
        json_data = {"error": {}}
        json_data = requests.get(
            meta_server + "/notification/list",
            params={"auth_token": auth_token, "type": notification_type},
        ).json()
        json_data = json_data["data"]
    except Exception as e:
        type_queue.put(error.error(e, json_data["error"]))
    type_queue.put(json_data)


notification_types = [
    "comments",
    "hyperchats",
    "comment_replies",
    "hyperchat_replies",
    "new_content",
    "livestream",
]


def sorter(element):
    return element["id"]


def list(meta_server, auth_token="", raw=False):
    # Get notifications
    error = ""

    all_data = []
    queues = []

    for notification_type in notification_types:
        queues.append(queue.Queue())
        args = [meta_server, auth_token, notification_type, queues[-1]]
        threading.Thread(None, get_type, None, args).start()

    for type_queue in queues:
        data = type_queue.get()
        if isinstance(data, str):
            error = data
        elif isinstance(data, type([])):
            all_data.extend(data)

    all_data.sort(key=sorter, reverse=True)

    if all_data == [] and error != "":
        return error
    if raw:
        return all_data
    return parse.notifications(all_data)


def unsync_account(account_ids, server="http://localhost:5279"):

    # This function will unsync the odysee wallet
    try:
        for item in account_ids:
            parameters = {"method": "account_remove"}
            parameters["params"] = {"account_id": item}
            account_remove = requests.post(server, json=parameters)
    except:
        return True
    return False


def sync_account(email, password, server="http://localhost:5279"):

    # This function will sync the odysee wallet.
    # Credit for the original functionality go to:
    # Blender Dumbass and TrueAuraCoral

    odysee_api = "https://api.odysee.com/user/"
    lbry_api = "https://api.lbry.com/sync/get"

    # For signout purposes we'll check the currently added accounts
    account_ids = []
    account_list = requests.post(server, json={"method": "account_list"})
    for item in account_list.json()["result"]["items"]:
        account_ids.append(item["id"])

    # We should get an auth token. If we don't, we abort the operation.
    try:
        request = requests.get(odysee_api + "new")
        data = request.json()
        auth_token = data["data"]["auth_token"]
    except:
        return "No authentication token"

    # Next step is authenticate this auth-token that we just got.

    parameters = {"auth_token": auth_token, "email": email}
    parameters["password"] = password

    request = requests.post(odysee_api + "signin", params=parameters)
    if isinstance(request, str):
        return "Problem with Odysee server: Try again later."
    elif not request.json()["success"]:
        return "Email or Password wrong: Please try again."

    # Now we have the authenticated auth-token.
    # Now we can go to the next step.

    wallet_hash = requests.post(server, json={"method": "sync_hash"})

    # Next step. To retrieve the wallet from Odysee.

    hash_data = {"auth_token": auth_token, "hash": wallet_hash.json()["result"]}

    try:
        request = requests.post(lbry_api, data=hash_data)
        data = request.json()["data"]["data"]
    except:
        return "Problem with LBRY server: Try again later."

    # And the last step. Applying the sync.

    parameters = {"password": "", "blocking": True, "data": data}
    wallet_hash = requests.post(
        server, json={"method": "sync_apply", "params": parameters}
    )

    # Here we get the added accounts.
    # This will remove the ones already existing in the list.
    # We return this list along with AuthToken in return statement.

    account_list = requests.post(server, json={"method": "account_list"})
    for item in account_list.json()["result"]["items"]:
        if item["id"] in account_ids:
            account_ids.remove(item["id"])
            continue
        account_ids.append(item["id"])

    return {"auth_token": auth_token, "account_ids": account_ids}
