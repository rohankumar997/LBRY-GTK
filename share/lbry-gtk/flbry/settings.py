################################################################################
# LBRY-GTK Internals                                                           #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import requests

from flbry import error


def get(server="http://localhost:5279"):
    settings = {}
    try:
        json_data = {"error": {}}
        json_data = requests.post(
            server,
            json={"method": "preference_get", "params": {}},
        ).json()
        json_data = json_data["result"]
    except Exception as e:
        return error.error(e, json_data["error"])

    settings["preferences"] = json_data

    try:
        json_data = {"error": {}}
        json_data = requests.post(
            server,
            json={"method": "settings_get", "params": {}},
        ).json()
        json_data = json_data["result"]
    except Exception as e:
        return error.error(e, json_data["error"])

    settings["settings"] = json_data

    return settings


def set(settings, server="http://localhost:5279"):
    new_settings = {"preferences": {}, "settings": {}}
    for key in settings["preferences"]:
        try:
            json_data = {"error": {}}
            json_data = requests.post(
                server,
                json={
                    "method": "preference_set",
                    "params": {
                        "key": key,
                        "value": settings["preferences"][key],
                    },
                },
            ).json()
            json_data = json_data["result"]
        except Exception as e:
            return error.error(e, json_data["error"])
        new_settings["preferences"][key] = json_data

    for key in settings["settings"]:
        if not (
            isinstance(settings["settings"][key], list) or key == "jurisdiction"
        ):
            try:
                json_data = {"error": {}}
                json_data = requests.post(
                    server,
                    json={
                        "method": "settings_set",
                        "params": {
                            "key": key,
                            "value": settings["settings"][key],
                        },
                    },
                ).json()
                json_data = json_data["result"]
            except Exception as e:
                return error.error(e, json_data["error"])
            new_settings["settings"][key] = json_data
        else:
            new_settings["settings"][key] = settings["settings"][key]

    return new_settings
