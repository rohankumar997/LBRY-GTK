################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import gi, gc

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk, Pango

from Source import MDFill, Places

CssProvider = Gtk.CssProvider.new()
CssProvider.load_from_data(b"*:not(selection) {background: unset;}")

StyleContext = Gtk.StyleContext()
WidgetPath = Gtk.WidgetPath()
WidgetPath.append_type(Gtk.Button)
StyleContext.set_path(WidgetPath)
LinkColor = StyleContext.get_color(Gtk.StateFlags.LINK)

Types = [Gdk.BUTTON_PRIMARY, Gdk.BUTTON_MIDDLE]


class Markdown:
    def __init__(
        self,
        Window,
        GetPublication,
        Text,
        Path,
        IfComment,
        Markdown,
        AddPage,
        NoResolve=False,
    ):
        Builder = Gtk.Builder.new_from_file(Places.GladeDir + "Markdown.glade")
        Builder.connect_signals(self)
        self.Window = Window
        self.GetPublication, self.Text, self.Path = GetPublication, Text, Path
        self.IfComment, self.Markdown = IfComment, Markdown
        self.AddPage, self.NoResolve = AddPage, NoResolve
        self.Document = Builder.get_object("Document")
        self.TextBox = Builder.get_object("TextBox")
        self.Scrolled = Builder.get_object("Scrolled")
        self.ActivateAction = Builder.get_object("ActivateAction")

        StyleContext = self.TextBox.get_style_context()
        StyleContext.add_provider(
            CssProvider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        )
        Display = Gdk.Display.get_default()
        self.TextCursor = Gdk.Cursor.new_from_name(Display, "text")
        self.PointerCursor = Gdk.Cursor.new_from_name(Display, "pointer")
        TextBuffer = self.TextBox.get_buffer()
        Link = TextBuffer.create_tag("Link")
        Link.set_property("foreground-rgba", LinkColor)
        Link.set_property("underline", Pango.Underline.SINGLE)
        for Level in range(10):
            Quote = TextBuffer.create_tag("Quote" + str(Level + 1))
            Quote.set_property("left-margin", (Level + 1) * 30)

    def Fill(self):
        Overlay = self.Scrolled.get_parent()
        Overlay.remove(self.Scrolled)
        self.Document.remove(Overlay)
        NewOverlay = Gtk.Overlay.new()
        NewOverlay.add(self.Scrolled)
        self.Document.add(NewOverlay)

        if not self.Markdown:
            self.TextBox.get_buffer().set_text(self.Text)
            self.Document.show_all()
            return
        self.Links = []
        MDFill.Fill(
            self.Text,
            self.TextBox,
            self.Window,
            self.Links,
            self.IfComment,
            self.GetPublication,
            self.Path,
            self.NoResolve,
            self.AddPage,
            self,
        )
        if not self.IfComment:
            self.Adjustment.set_value(self.Adjustment.get_lower())
        self.Document.show_all()

    def SelectNext(self):
        for Index in range(len(self.Links)):
            Tag = self.Links[Index]
            if Tag.get_property("underline") == Pango.Underline.DOUBLE:
                if Index == len(self.Links) - 1:
                    break
                NextTag = self.Links[Index + 1]
                NextTag.set_property("underline", Pango.Underline.DOUBLE)
                Tag.set_property("underline", Pango.Underline.SINGLE)
                break

    def SelectPrevious(self):
        for Index in range(len(self.Links)):
            Tag = self.Links[Index]
            if Tag.get_property("underline") == Pango.Underline.DOUBLE:
                if 0 == Index:
                    break
                NextTag = self.Links[Index - 1]
                NextTag.set_property("underline", Pango.Underline.DOUBLE)
                Tag.set_property("underline", Pango.Underline.SINGLE)
                break

    def Activate(self, Button):
        for Index in range(len(self.Links)):
            Tag = self.Links[Index]
            if Tag.get_property("underline") == Pango.Underline.DOUBLE:
                self.TextBox.set_name(Tag.get_property("name"))
                Event = Gdk.Event.new(Gdk.EventType.BUTTON_RELEASE)
                Event.button = Types[Button]
                Event.window = self.TextBox.get_window(Gtk.TextWindowType.TEXT)
                Buffer = self.TextBox.get_buffer()
                Iter = Buffer.get_start_iter()
                Tag.event(self.TextBox, Event, Iter)
                break

    def on_TextBox_motion_notify_event(self, Widget, Event):
        Location = Widget.window_to_buffer_coords(
            Gtk.TextWindowType.TEXT, Event.x, Event.y
        )
        Iter = Widget.get_iter_at_location(Location[0], Location[1])
        Window = Widget.get_window(Gtk.TextWindowType.TEXT)
        if Iter[0] == False:
            Widget.set_name("")
            Widget.set_tooltip_text("")
            Window.set_cursor(self.TextCursor)
            return
        Iter, TagTable = Iter[1], Widget.get_buffer().get_tag_table()
        if Iter.has_tag(TagTable.lookup("Link")):
            Tags = Iter.get_tags()
            for Tag in Tags:
                Name = Tag.get_property("name")
                if (
                    isinstance(Name, str)
                    and Name != ""
                    and Name != "Link"
                    and not "Quote" in Name
                ):
                    Widget.set_tooltip_text(Name.split(" ")[0])
                    break
            Window.set_cursor(self.PointerCursor)
            return
        Widget.set_name("")
        Widget.set_tooltip_text("")
        Window.set_cursor(self.TextCursor)

    def on_TextBox_draw(self, Widget, Discard=""):
        Width = Widget.get_allocated_width() - 1
        Height = Widget.get_preferred_height_for_width(Width).minimum_height
        Widget.set_size_request(-1, Height / 2)

    def on_TextBox_grab_focus(self, Widget, Discard=""):
        if not self.IfComment:
            self.ScrollPlace = self.Adjustment.get_value()

    def on_TextBox_focus_in_event(self, Widget, Discard=""):
        if not self.IfComment:
            self.Adjustment.set_value(self.ScrollPlace)
