################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import threading, json, gi

gi.require_version("Gtk", "3.0")
from gi.repository import GLib, Gtk

from flbry import comments

from Source import Places, Popup


class CommentControlMiddle:
    def __init__(self, *args):
        (
            self.Window,
            self.Row,
            self.Reactions,
            self.Channelser,
            self.Edit,
            self.CommentServer,
            self.ReplyScrolled,
            self.Post,
            self.PreviewBox,
            self.Tip,
            self.TipLabel1,
            self.TipLabel2,
            self.PChannel,
            self.ChannelList,
            self.EditImage,
        ) = args
        Builder = Gtk.Builder.new_from_file(
            Places.GladeDir + "CommentControlMiddle.glade"
        )
        Builder.connect_signals(self)
        self.CommentID = self.Row[4]
        self.CommentControlMiddle = Builder.get_object("CommentControlMiddle")
        self.Heart = Builder.get_object("Heart")
        self.Like = Builder.get_object("Like")
        self.Dislike = Builder.get_object("Dislike")
        self.LikeNumber = Builder.get_object("LikeNumber")
        self.DislikeNumber = Builder.get_object("DislikeNumber")
        self.Reply = Builder.get_object("Reply")
        self.ChannelsBox1 = Builder.get_object("ChannelsBox1")

        self.Like.set_active(self.Reactions[2])
        self.Dislike.set_active(self.Reactions[3])
        self.Heart.set_active(self.Reactions[4])

        self.LikeNumber.set_label(" %s " % str(self.Reactions[0]))
        self.DislikeNumber.set_label(" %s " % str(self.Reactions[1]))

    def React(self, Channel, Type, Remove):
        with open(Places.ConfigDir + "Session.json", "r") as File:
            Session = json.load(File)
        ErrorOrData = comments.reaction_react(
            *Channel,
            self.CommentServer,
            str(self.CommentID),
            Type,
            Remove,
            server=Session["Server"]
        )
        if isinstance(ErrorOrData, str):
            Popup.Error(ErrorOrData, self.Window)
            return ""
        ErrorOrData = comments.reaction_list(
            *Channel,
            self.CommentServer,
            str(self.CommentID),
            server=Session["Server"]
        )
        if isinstance(ErrorOrData, str):
            Popup.Error(ErrorOrData, self.Window)
            return ""
        return ErrorOrData

    def on_Heart_button_press_event(self, Widget, Event=""):
        Remove = [Widget.get_active()]
        threading.Thread(None, self.HeartThread, None, Remove).start()

    def on_Parent_button_press_event(self, Widget, Event=""):
        Widget.get_parent().event(Event)

    def HeartThread(self, Remove):
        Channel = []
        for ChannelItem in self.ChannelList:
            if ChannelItem[3] == self.PChannel:
                Channel = [ChannelItem[0], ChannelItem[-1]]
                break
        if Channel == []:
            return
        ErrorOrData = self.React(Channel, "creator_like", Remove)
        if not isinstance(ErrorOrData, str):
            GLib.idle_add(self.LikeDislikeUpdate, Channel, ErrorOrData)

    def on_Dislike_button_press_event(self, Widget, Event=""):
        self.LikeDislike("dislike", Widget.get_active())

    def on_Like_button_press_event(self, Widget, Event=""):
        self.LikeDislike("like", Widget.get_active())

    def LikeDislike(self, Type, Active):
        ChannelRow = self.Channelser.Get()
        Channel = [ChannelRow[0], ChannelRow[-1]]
        args = [Channel, Type, Active]
        threading.Thread(None, self.LikeDislikeThread, None, args).start()

    def LikeDislikeThread(self, Channel, Type, Remove):
        ErrorOrData = self.React(Channel, Type, Remove)
        if not isinstance(ErrorOrData, str):
            GLib.idle_add(self.LikeDislikeUpdate, Channel, ErrorOrData)

    def LikeDislikeUpdate(self, Channel, Data):
        MyLike = Data["my_reactions"][self.CommentID]["like"]
        MyDislike = Data["my_reactions"][self.CommentID]["dislike"]
        OtherLike = Data["others_reactions"][self.CommentID]["like"]
        OtherDislike = Data["others_reactions"][self.CommentID]["dislike"]
        Heart = Data["my_reactions"][self.CommentID]["creator_like"]
        Heart += Data["others_reactions"][self.CommentID]["creator_like"]
        self.Like.set_active(MyLike == 1)
        self.Dislike.set_active(MyDislike == 1)
        self.LikeNumber.set_label(" %s " % str(OtherLike + MyLike))
        self.DislikeNumber.set_label(" %s " % str(OtherDislike + MyDislike))
        self.Heart.set_active(Heart == 1)

    def on_Reply_button_press_event(self, Widget, Event=""):
        if self.Edit.get_image() == self.EditImage:
            Visible = self.ReplyScrolled.get_visible()
            ToSet = [self.ReplyScrolled, self.Tip, self.PreviewBox.get_parent()]
            ToSet.extend([self.Post, self.TipLabel1, self.TipLabel2])
            for Widget in ToSet:
                Widget.set_visible(not Visible)
            self.ReplyScrolled.set_name(str(not Visible))

    def NoToggleBefore(self, Widget):
        Widget.set_name(str(Widget.get_active()))

    def NoToggleAfter(self, Widget):
        Widget.set_active(Widget.get_name() == "True")
