################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import gi, threading, json, copy

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GLib

from flbry import channel, main

from Source import Places, Popup, Settings, Move, SelectUtil
from Source.DateTime import DateTime
from Source.Tag import Tag
from Source.Select import Select
from Source.Channels import Channels
from Source.Language import Language
from Source.Icons import LBCLabel

Message = "You have to choose a license or set License Type to Custom."


class NewPublication:
    def __init__(self, Window, Title):
        Builder = Gtk.Builder.new_from_file(
            Places.GladeDir + "NewPublication.glade"
        )
        Builder.connect_signals(self)
        self.Window, self.Title = Window, Title
        self.NewPublication = Builder.get_object("NewPublication")
        self.ChannelsBox = Builder.get_object("ChannelsBox")
        self.Name = Builder.get_object("Name")
        self.PTitle = Builder.get_object("Title")
        self.File = Builder.get_object("File")
        self.Description = Builder.get_object("Description")
        self.ThumbnailType = Builder.get_object("ThumbnailType")
        self.LicenseType = Builder.get_object("LicenseType")
        self.ThumbnailUrl = Builder.get_object("ThumbnailUrl")
        self.ThumbnailFile = Builder.get_object("ThumbnailFile")
        self.TagBox = Builder.get_object("TagBox")
        self.Deposit = Builder.get_object("Deposit")
        self.Price = Builder.get_object("Price")
        self.LanguageBox = Builder.get_object("LanguageBox")
        self.License = Builder.get_object("License")
        self.LicenseLabel = Builder.get_object("LicenseLabel")
        self.LicenseModel = Builder.get_object("LicenseModel")
        self.DateTimeBox = Builder.get_object("DateTimeBox")
        self.Create = Builder.get_object("Create")
        self.Clear = Builder.get_object("Clear")
        self.FileActive = Builder.get_object("FileActive")
        self.ThumbnailTypeActive = Builder.get_object("ThumbnailTypeActive")
        self.ThumbnailFileActive = Builder.get_object("ThumbnailFileActive")
        self.LicenseTypeActive = Builder.get_object("LicenseTypeActive")
        self.SelectPrevious = Builder.get_object("SelectPrevious")
        self.SelectNext = Builder.get_object("SelectNext")
        self.OpenOnCurrent = Builder.get_object("OpenOnCurrent")
        self.OpenOnNew = Builder.get_object("OpenOnNew")
        self.MoveLeft = Builder.get_object("MoveLeft")
        self.MoveRight = Builder.get_object("MoveRight")
        self.MoveUp = Builder.get_object("MoveUp")
        self.MoveDown = Builder.get_object("MoveDown")
        self.TagSelect = Builder.get_object("TagSelect")
        self.OSI = Builder.get_object("OSI")
        self.FSF = Builder.get_object("FSF")
        self.LicenseName = Builder.get_object("LicenseName")
        self.LicenseNameLabel = Builder.get_object("LicenseNameLabel")
        self.LicenseURL = Builder.get_object("LicenseURL")
        self.LicenseURLLabel = Builder.get_object("LicenseURLLabel")

        self.Deposit.get_parent().get_children()[1].set_from_pixbuf(LBCLabel)
        self.Price.get_parent().get_children()[1].set_from_pixbuf(LBCLabel)

        ChannelData = self.GetChannelData()
        self.Channelser = Channels(False, self.Window, *ChannelData)
        self.Channels = self.Channelser.Channels
        self.ChannelsBox.pack_start(self.Channelser.ChannelsBox, True, True, 0)

        self.DateTimer = DateTime()
        self.DateTimeBox.pack_start(self.DateTimer.DateTime, True, True, 0)

        self.Tagger = Tag(True)
        self.TagBox.pack_start(self.Tagger.Tag, True, True, 0)

        self.Languager = Language()
        self.LanguageBox.pack_start(self.Languager.Language, True, True, 0)

        with open(Places.JsonDir + "Licenses.json") as Licenses:
            self.Licenses = json.load(Licenses)["licenses"]
        for License in self.Licenses:
            try:
                OSI = License["isOsiApproved"]
            except:
                OSI = False
            try:
                FSF = License["isFsfLibre"]
            except:
                FSF = False

            self.LicenseModel.append(
                [License["name"], License["seeAlso"][0], OSI, FSF]
            )

        DateTimer = self.DateTimer
        Widgets = [self.Channels, self.Channelser.Active, self.Name]
        Widgets.extend([self.PTitle, self.File, self.FileActive])
        Widgets.extend([self.Description, self.ThumbnailType])
        Widgets.extend([self.ThumbnailTypeActive, self.ThumbnailUrl])
        Widgets.extend([self.ThumbnailFile, self.ThumbnailFileActive])
        Widgets.extend([self.TagBox, self.Deposit, self.Price])
        Widgets.extend([self.DateTimeBox, DateTimer.DateActive, DateTimer.Hour])
        Widgets.extend([DateTimer.Minute, DateTimer.Second])
        Widgets.extend([DateTimer.DateReset, DateTimer.DateUse])
        Widgets.extend([DateTimer.DateUseActive, self.LanguageBox])
        Widgets.extend([self.LicenseType, self.LicenseTypeActive, self.License])
        Widgets.extend([self.LicenseName, self.LicenseURL, self.Create])
        Widgets.extend([self.Clear])
        Items = SelectUtil.PrepareWidgets(
            Widgets, [self.Tagger], [self.Languager], [DateTimer]
        )
        self.Selector = Select(*Items)

    def StopScroll(self, Widget, Event):
        Widget.emit_stop_by_name("scroll-event")

    def on_OpenOnCurrent_activate(self, Widget):
        if self.NewPublication.get_realized():
            self.Selector.Activate(0)

    def on_OpenOnNew_activate(self, Widget):
        if self.NewPublication.get_realized():
            self.Selector.Activate(1)

    def on_SelectPrevious_activate(self, Widget):
        if self.NewPublication.get_realized():
            self.Selector.Backward()

    def on_SelectNext_activate(self, Widget):
        if self.NewPublication.get_realized():
            self.Selector.Forward()

    def on_TagSelect_activate(self, Widget):
        if self.NewPublication.get_realized():
            if self.Selector.CurrentItem == 8:
                self.Tagger.Select()
            elif self.Selector.CurrentItem == 17:
                self.Languager.Tagger.Select()

    def on_MoveLeft_activate(self, Widget):
        if self.NewPublication.get_realized():
            if self.Selector.CurrentItem == 8:
                Move.FlowBoxLeftRight(self.Tagger.FlowBox, -1)
            elif self.Selector.CurrentItem == 17:
                Move.FlowBoxLeftRight(self.Languager.Tagger.FlowBox, -1)

    def on_MoveRight_activate(self, Widget):
        if self.NewPublication.get_realized():
            if self.Selector.CurrentItem == 8:
                Move.FlowBoxLeftRight(self.Tagger.FlowBox)
            elif self.Selector.CurrentItem == 17:
                Move.FlowBoxLeftRight(self.Languager.Tagger.FlowBox)

    def on_MoveDown_activate(self, Widget):
        if self.NewPublication.get_realized():
            if self.Selector.CurrentItem == 8:
                Move.FlowBoxUpDown(self.Tagger.FlowBox)
            elif self.Selector.CurrentItem == 17:
                Move.FlowBoxUpDown(self.Languager.Tagger.FlowBox)

    def on_MoveUp_activate(self, Widget):
        if self.NewPublication.get_realized():
            if self.Selector.CurrentItem == 8:
                Move.FlowBoxUpDown(self.Tagger.FlowBox, -1)
            elif self.Selector.CurrentItem == 17:
                Move.FlowBoxUpDown(self.Languager.Tagger.FlowBox, -1)

    def GetChannelData(self):
        try:
            LBRYSettings = Settings.Get()
            if isinstance(LBRYSettings, str):
                return []
        except:
            return []
        LBRYGTKSettings = LBRYSettings["preferences"]["shared"]["value"][
            "LBRY-GTK"
        ]
        Session = LBRYSettings["Session"]
        ChannelList = channel.channel_list(server=Session["Server"])
        return [ChannelList, LBRYGTKSettings]

    def ShowNewPublication(self):
        ChannelData = self.GetChannelData()
        if ChannelData == "":
            return
        self.Channelser.Update(*ChannelData)
        self.on_Clear_button_press_event()
        self.on_ThumbnailType_changed(self.ThumbnailType)
        self.on_LicenseType_changed(self.LicenseType)

        self.Replace("NewPublication")
        self.Title.set_text("New Publication")

    def on_LicenseEntry_changed(self, Widget):
        NewLicense, ID = Widget.get_text(), 0
        for License in self.Licenses:
            if NewLicense == License["name"]:
                self.License.set_active(ID)
                break
            ID += 1
        Active, OSI, FSF = self.License.get_active(), False, False
        if Active != -1:
            Path = Gtk.TreePath.new_from_indices([Active])
            Iter = self.LicenseModel.get_iter(Path)
            OSI = self.LicenseModel.get_value(Iter, 2)
            FSF = self.LicenseModel.get_value(Iter, 3)

        self.OSI.set_active(OSI)
        self.FSF.set_active(FSF)

    def NoToggleBefore(self, Widget):
        Widget.set_name(str(Widget.get_active()))

    def NoToggleAfter(self, Widget):
        Widget.set_active(Widget.get_name() == "True")

    def on_ThumbnailType_changed(self, Widget):
        if Widget.get_active() == 0:
            self.ThumbnailUrl.set_visible(False)
            self.ThumbnailFile.set_visible(True)
        else:
            self.ThumbnailUrl.set_visible(True)
            self.ThumbnailFile.set_visible(False)

    def on_LicenseType_changed(self, Widget):
        Active = Widget.get_active() == 0
        self.License.set_visible(Active)
        self.LicenseLabel.set_visible(Active)
        self.LicenseName.set_visible(not Active)
        self.LicenseNameLabel.set_visible(not Active)
        self.LicenseURL.set_visible(not Active)
        self.LicenseURLLabel.set_visible(not Active)

    def on_Clear_button_press_event(self, Widget="", Event=""):
        self.License.set_active(-1)
        self.Languager.Combo.set_active(0)
        self.ThumbnailType.set_active(0)
        self.Tagger.RemoveAll()
        self.Languager.Tagger.RemoveAll()
        self.Name.set_text("")
        self.PTitle.set_text("")
        self.ThumbnailUrl.set_text("")
        self.Tagger.Entry.set_text("")
        self.Deposit.set_value(0.1)
        self.Price.set_value(0)
        self.File.set_filename("")
        self.ThumbnailFile.set_filename("")
        self.DateTimer.on_DateReset_button_press_event()
        self.DateTimer.DateUse.set_active(False)
        Buffer = self.Description.get_buffer()
        Buffer.set_text("")

    def on_Create_button_press_event(self, Widget, Event):
        Channel, Title = self.Channelser.Get()[0], self.PTitle.get_text()
        Name, File = self.Name.get_text(), self.File.get_filename()

        Buffer = self.Description.get_buffer()
        Start, End = Buffer.get_start_iter(), Buffer.get_end_iter()

        Description = Buffer.get_text(Start, End, True)

        ThumbnailFile, ThumbnailUrl = "", ""
        if self.ThumbnailType.get_active() == 0:
            ThumbnailFile = self.ThumbnailFile.get_filename()
        else:
            ThumbnailUrl = self.ThumbnailUrl.get_text()

        Deposit, Tags = self.Deposit.get_value(), self.Tagger.Tags
        ReleaseTime, Price = self.DateTimer.GetTime(), self.Price.get_value()

        Languages = copy.deepcopy(self.Languager.Tagger.Tags)
        for Index in range(len(Languages)):
            Languages[Index] = Languages[Index].split("/")[0]

        if self.LicenseType.get_active() == 0:
            Active = self.License.get_active()
            if Active == -1:
                Popup.Message(Message, self.Window)
                return
            Path = Gtk.TreePath.new_from_indices([Active])
            Iter = self.LicenseModel.get_iter(Path)
            License = self.LicenseModel.get_value(Iter, 0)
            LicenseUrl = self.LicenseModel.get_value(Iter, 1)
        else:
            License = self.LicenseName.get_text()
            LicenseUrl = self.LicenseURL.get_text()

        args = {
            "name": Name,
            "bid": Deposit,
            "file_path": File,
            "title": Title,
            "license": License,
            "license_url": LicenseUrl,
            "thumbnail_url": ThumbnailUrl,
            "thumbnail_file": ThumbnailFile,
            "channel_name": Channel,
            "description": Description,
            "tags": Tags,
            "fee_amount": Price,
            "languages": Languages,
            "release_time": ReleaseTime,
        }

        threading.Thread(None, self.UploadThread, None, [args]).start()

    def UploadThread(self, args):
        with open(Places.ConfigDir + "Session.json", "r") as File:
            Session = json.load(File)

        JsonData = main.publish(**args, server=Session["Server"])

        if isinstance(JsonData, str):
            Popup.Error(JsonData, self.Window)
            return
        Popup.Message("Publication successful.", self.Window)
