################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import re, markdown

Entities = {
    "&amp;": "&",
    "&lt;": "<",
    "&gt;": ">",
    "&quot;": '"',
    "&apos;": "'",
}


def Replace(Match, MDUrls):
    String = Match.string[Match.start() : Match.end() - 1]
    for MDUrl in MDUrls:
        if MDUrl.start() <= Match.start() and Match.end() <= MDUrl.end():
            return String
    if Match.end() == len(Match.string):
        String = Match.group()
    End = Match.string[Match.end() - 1]
    if String.endswith("."):
        End = String[-1] + End
        String = String[:-1]
    if String.find("://") + 3 == len(String):
        return String + End
    return "[%s](%s)%s" % (String, String, End)


def Parenthese(Match):
    String = Match.group()
    Url = String[String.find("](") + 2 : -1].replace("(", "\(")
    return String[: String.find("](") + 2] + Url + ")"


def Quote(Match):
    if Match.string[Match.start() - 1] == "\n" or Match.start() == 0:
        return int((Match.end() - Match.start()) / 4) * ">"
    else:
        return Match.group()


Url = "(?<!\]\()((?:https?|s?ftps?|lbry):\/\/[^\s\]]+)(?:[\s\]\)](?!\()|$)"
TitleUrl = "\[(.*?)\]\((\S*?) ?([\"'](.*?)[\"'])?\)"
Paragraph = "(<%s>\n<p>)([\s\S]*?(?=</p))</p>"


def Parse(Markdown):
    for Entity in Entities:
        Markdown = Markdown.replace(Entities[Entity], Entity)
    Markdown = re.sub("&lt;([^(&gt;)]*)&gt;", "<\\1>", Markdown)
    Markdown = re.sub("(&gt;)+", Quote, Markdown)

    Markdown = re.sub(TitleUrl, "[\\1](\\2)", Markdown)
    MDUrls = re.finditer(TitleUrl, Markdown)
    Markdown = re.sub("#([^# ])", "\\#\\1", Markdown)

    Markdown = re.sub(Url, lambda Match: Replace(Match, MDUrls), Markdown)
    Markdown = re.sub(TitleUrl, Parenthese, Markdown)
    Text = markdown.markdown(Markdown, extensions=["fenced_code", "nl2br"])
    Text = re.sub(' class=".*"', "", re.sub(' alt="[^"]*"', "", Text))
    Text = re.sub(' width="[^"]*"', "", re.sub(' height="[^"]*"', "", Text))
    Text = re.sub("~~([^~]*)~~", "<s>\\1</s>", Text)

    NoPs = ["li", "blockquote"]
    for NoP in NoPs:
        Text = re.sub(Paragraph % NoP, "<%s>\\2" % NoP, Text)
    Text = re.sub("<blockquote>", "<br/><blockquote>", Text)
    Text = re.sub("</p>", "<br/>", re.sub("<p[^>]*>", "<br/>", Text))
    Text = re.sub("<br/>\n</blockquote>", "</blockquote>", Text)
    Text = re.sub("<h1>", "<br/>" + 6 * "<big>", Text)
    Text = re.sub("</h1>", 6 * "</big>" + "<br/>", Text)
    Text = re.sub("<h2>", "<br/>" + 5 * "<big>", Text)
    Text = re.sub("</h2>", 5 * "</big>" + "<br/>", Text)
    Text = re.sub("<h3>", "<br/>" + 4 * "<big>", Text)
    Text = re.sub("</h3>", 4 * "</big>" + "<br/>", Text)
    Text = re.sub("<h4>", "<br/>" + 3 * "<big>", Text)
    Text = re.sub("</h4>", 3 * "</big>" + "<br/>", Text)
    Text = re.sub("<h5>", "<br/><big><big>", Text)
    Text = re.sub("</h5>", "</big></big><br/>", Text)
    Text = re.sub("</h6>", "</big><br/>", re.sub("<h6>", "<br/><big>", Text))
    Text = re.sub("</strong>", "</b>", re.sub("<strong>", "<b>", Text))
    Text = re.sub("</em>", "</i>", re.sub("<em>", "<i>", Text))
    Text = re.sub("</code>", "</tt>", re.sub("<code>", "<tt>", Text))
    Text = re.sub("</li>", "", re.sub("<img", " <img", Text))

    Text = re.sub("</?module>", "", re.sub("</?click>", "", Text))
    Text = re.sub("</?stkr>", "", re.sub("</?center>", "", Text))
    Text = re.sub("</?pre>", "", re.sub("<font[^>]*>", "", Text))
    Text = re.sub("</font>", "", re.sub("    ", "\t", Text))
    Lines = Text.split("\n")

    CodeBlock, ListLevel, ListTypes, Counters = False, -1, [], []
    for Index in range(len(Lines)):
        if "<tt>" in Lines[Index] and not "</tt>" in Lines[Index]:
            CodeBlock = True
            Lines[Index] += "</tt><br/>"
            continue
        if CodeBlock:
            Lines[Index] = "<tt>" + Lines[Index]
            if "</tt>" in Lines[Index]:
                CodeBlock = False
            else:
                Lines[Index] += "</tt><br/>"
        else:
            if re.match("^(\t*)-", Lines[Index]):
                Lines[Index] = re.sub("^(\t*)-", "<br/>\\1•", Lines[Index])
            if re.match("^(\t*)([0-9]+)", Lines[Index]):
                Lines[Index] = re.sub(
                    "^(\t*)([0-9]+)", "<br/>\\1\\2", Lines[Index]
                )
            if (
                re.match("^<br/>=+<br/>$", Lines[Index])
                and Index != 0
                and not "<big>" in Lines[Index - 1]
            ):
                Lines[Index - 1] = (
                    "<br/>" + 6 * "<big>" + Lines[Index - 1] + 6 * "</big>"
                )
                Lines[Index] = ""
            if (
                re.match("^<br/>-+<br/>$", Lines[Index])
                and Index != 0
                and not "<big>" in Lines[Index - 1]
            ):
                Lines[Index - 1] = (
                    "<br/>" + 5 * "<big>" + Lines[Index - 1] + 5 * "</big>"
                )
                Lines[Index] = ""
            if (
                "<ul>" in Lines[Index]
                and "<li>" in Lines[Index]
                and Lines[Index].find("<li>") < Lines[Index].find("<ul>")
            ):
                Sign = "• "
                if ListTypes[-1] == "O":
                    Sign = str(Counters[-1]) + ". "
                    Counters[-1] += 1
                Lines[Index] = re.sub(
                    "<li>", ListLevel * "\t" + Sign, Lines[Index]
                )
                ListLevel += 1
                Lines[Index] = re.sub("<ul>", "", Lines[Index])
                Lines[Index] = "<br/>" + Lines[Index]
                ListTypes.append("U")
            elif (
                "<ol>" in Lines[Index]
                and "<li>" in Lines[Index]
                and Lines[Index].find("<li>") < Lines[Index].find("<ol>")
            ):
                Sign = "• "
                if ListTypes[-1] == "O":
                    Sign = str(Counters[-1]) + ". "
                    Counters[-1] += 1
                Lines[Index] = re.sub(
                    "<li>", ListLevel * "\t" + Sign, Lines[Index]
                )
                ListLevel += 1
                Lines[Index] = re.sub("<ol>", "", Lines[Index])
                Lines[Index] = "<br/>" + Lines[Index]
                ListTypes.append("O")
                Counters.append(1)
            elif "<ul>" in Lines[Index]:
                ListTypes.append("U")
                ListLevel += 1
                Lines[Index] = re.sub("<ul>", "", Lines[Index])
            elif "<ol>" in Lines[Index]:
                ListTypes.append("O")
                Counters.append(1)
                ListLevel += 1
                Lines[Index] = re.sub("<ol>", "", Lines[Index])
            elif "</ul>" in Lines[Index] or "</ol>" in Lines[Index]:
                if "</ol>" in Lines[Index]:
                    Counters.pop()
                ListTypes.pop()
                ListLevel -= 1
                Lines[Index] = re.sub(
                    "</ul>", "", re.sub("</ol>", "", Lines[Index])
                )
            if ListLevel != -1 and "<li>" in Lines[Index]:
                Sign = "• "
                if ListTypes[-1] == "O":
                    Sign = str(Counters[-1]) + ". "
                    Counters[-1] += 1
                Lines[Index] = re.sub(
                    "<li>", ListLevel * "\t" + Sign, Lines[Index]
                )
                Lines[Index] = "<br/>" + Lines[Index]
    return re.sub("<br\\s*/?>", "\n", " ".join(Lines)).strip()
