################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################


import gi

gi.require_version("Gtk", "3.0")
from gi.repository import GLib

from datetime import datetime
from datetime import timedelta


class ShowHide:
    Hidden, HiddenTime = False, datetime.now()

    def __init__(self, WidgetAndFunctions, Time):
        self.WidgetAndFunctions = WidgetAndFunctions
        self.Time = timedelta(milliseconds=Time)
        GLib.timeout_add(10, self.Show)

    def Show(self):
        if self.Hidden and self.Time < datetime.now() - self.HiddenTime:
            for WidgetAndFunction in self.WidgetAndFunctions:
                WidgetAndFunction[1](WidgetAndFunction[0])
            self.Hidden = False
        return True

    def Hide(self):
        if not self.Hidden:
            for WidgetAndFunction in self.WidgetAndFunctions:
                WidgetAndFunction[2](WidgetAndFunction[0])
            self.Hidden = True
        self.HiddenTime = datetime.now()
