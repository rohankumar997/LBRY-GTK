################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import gi, requests, os.path, threading, multiprocessing, time

gi.require_version("GdkPixbuf", "2.0")
from gi.repository import GdkPixbuf

from PIL import Image

from Source import Places

if multiprocessing.current_process().name == "MainProcess":
    gi.require_version("Gtk", "3.0")
    from gi.repository import Gtk, Gdk, GdkPixbuf, GLib

    GoodTypes = GdkPixbuf.Pixbuf.get_formats()
    for Index in range(len(GoodTypes)):
        GoodTypes[Index] = GoodTypes[Index].get_name()

Forbidden = ["<", ">", ":", '"', "\\", "/", "|", "?", "*"]


def Path(Url):
    Name = Url
    for Character in Forbidden:
        Name = Name.replace(Character, "_")
    return Places.CacheDir + Name


def GetResolution(Data, ResolutionFunction, Function, *Args):
    Queue = multiprocessing.Queue()
    Process = multiprocessing.Process(
        None, ResolutionFunction, None, (Data, Queue), daemon=True
    )
    Process.start()
    Process.join(60)
    GLib.idle_add(Function, *Args, Queue.get())


def GetUrlResolution(Url, Function, *Args):
    GetResolution(Url, GetUrlResolutionProcess, Function, *Args)


def GetPathResolution(FilePath, Function, *Args):
    GetResolution(FilePath, GetPathResolutionProcess, Function, *Args)


def GetUrlResolutionProcess(Url, Queue):
    FilePath = Download(Url)
    if FilePath != "":
        GetPathResolutionProcess(FilePath, Queue)
    else:
        Queue.put((1, 1))


def GetPathResolutionProcess(FilePath, Queue):
    if os.path.exists(FilePath):
        if FilePath.endswith(".svg"):
            Info = GdkPixbuf.Pixbuf.get_file_info(FilePath)
            Size = (Info[1], Info[2])
        else:
            with Image.open(FilePath) as Img:
                Size = Img.size
        Queue.put(Size)
    else:
        Queue.put((1, 1))


def Download(Url):
    FilePath = Path(Url)
    if not os.path.exists(FilePath):
        try:
            with requests.get(Url, allow_redirects=True) as Request:
                if Request.ok:
                    with open(FilePath, "wb") as File:
                        File.write(Request.content)
            Img = Image.open(FilePath)
            Img.close()
            return FilePath
        except:
            pass
    else:
        Retries = 0
        while Retries < 100:
            try:
                if not FilePath.endswith(".svg"):
                    Img = Image.open(FilePath)
                    Img.close()
                return FilePath
            except:
                time.sleep(0.01)
                Retries += 1
    return ""


def FillPixbuf(Url, Pixbuf, Function="", *args):
    Pixbuf.fill(0x00000000)
    Args = [Url, Pixbuf, Function, *args]
    threading.Thread(
        None, FillPixbufDownloadThread, None, Args, daemon=True
    ).start()


def FillPixbufDownloadThread(Url, Pixbuf, Function, *args):
    Queue = multiprocessing.Queue()
    Process = multiprocessing.Process(
        target=FillPixbufDownloadProcess,
        args=(Url, Queue, GoodTypes),
        daemon=True,
    )
    Process.start()
    Process.join(60)

    if Process.exitcode == None:
        Process.kill()
        Process.join()
        FilePath = ""
    else:
        FilePath = Queue.get()
    GLib.idle_add(FillPixbufFill, FilePath, Pixbuf, Function, *args)


def FillPixbufDownloadProcess(Url, Queue, GoodTypes):
    FilePath = ""
    if Url != "":
        FilePath = Download(Url)
    try:
        with Image.open(FilePath) as Img:
            Format = Img.format.lower()
            if not Format in GoodTypes or Format == "gif":
                with Img.convert("RGBA") as NewImg:
                    NewImg.save(FilePath, "png")
    except:
        pass
    Queue.put(FilePath)


def FillPixbufFill(FilePath, Pixbuf, Function, *args):
    Width, Height = Pixbuf.get_width(), Pixbuf.get_height()

    Screen = Gdk.Screen.get_default()
    IconTheme = Gtk.IconTheme.get_for_screen(Screen)
    Loaded = IconTheme.load_icon(
        "image-missing", min(Width, Height), Gtk.IconLookupFlags.FORCE_SIZE
    )
    try:
        if FilePath != "":
            Loaded = GdkPixbuf.Pixbuf.new_from_file_at_size(
                FilePath, Width, Height
            )
    except:
        pass

    NewWidth, NewHeight = Loaded.get_width(), Loaded.get_height()
    GdkPixbuf.Pixbuf.composite(
        Loaded,
        Pixbuf,
        (Width - NewWidth) / 2,
        (Height - NewHeight) / 2,
        NewWidth,
        NewHeight,
        (Width - NewWidth) / 2,
        (Height - NewHeight) / 2,
        1,
        1,
        GdkPixbuf.InterpType.BILINEAR,
        255,
    )

    if Function != "":
        Function(*args)
