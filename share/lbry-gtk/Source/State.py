################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import json, threading


def DifferentLists(List1, List2):
    for Index in range(len(List1)):
        if List1[Index] != List2[Index]:
            return True
    return False


class State:
    def __init__(self):
        self.States, self.CurrentState = [], -1

    def Import(self, FunctionName, Data):
        if isinstance(Data, str):
            Data = json.loads(Data)
        if self.PossibleStates[FunctionName]:
            Function = self.PossibleStates[FunctionName]
            threading.Thread(None, Function, None, Data).start()

    def Export(self, Function, Data, String=False):
        if String:
            Data = json.dumps(Data)
        FunctionName = ""
        if isinstance(Function, str):
            FunctionName = Function
        else:
            for State in self.PossibleStates:
                if self.PossibleStates[State] == Function:
                    FunctionName = State
                    break
        return [FunctionName, Data]

    def Save(self, Function, Data, Title):
        NoCheck = False
        if self.CurrentState != -1 and self.CurrentState < len(self.States):
            OldState = self.States[self.CurrentState]
        else:
            NoCheck = True
        if (
            NoCheck
            or Function != OldState["Function"]
            or DifferentLists(Data, OldState["Data"])
        ):
            self.CurrentState += 1
            State = {
                "Function": Function,
                "Data": Data,
                "Title": Title,
                "Index": self.CurrentState,
            }
            if self.CurrentState < len(self.States):
                self.States[self.CurrentState] = State
            else:
                self.States.append(State)

    def Back(self):
        if 0 < self.CurrentState:
            self.Goto(self.CurrentState - 1)

    def Forward(self):
        if self.CurrentState < len(self.States) - 1:
            self.Goto(self.CurrentState + 1)

    def Previous(self):
        if 0 < self.CurrentState:
            Previous = self.States[self.CurrentState - 1]
            return self.Export(Previous["Function"], Previous["Data"])
        return ""

    def Next(self):
        if self.CurrentState < len(self.States) - 1:
            Next = self.States[self.CurrentState + 1]
            return self.Export(Next["Function"], Next["Data"])
        return ""

    def Before(self):
        List = []
        for Index in range(self.CurrentState):
            List.append(self.States[Index])
        return List

    def After(self):
        List = []
        for Index in range(self.CurrentState + 1, len(self.States)):
            List.append(self.States[Index])
        return List

    def Goto(self, Index):
        if -1 < Index and Index < len(self.States):
            self.CurrentState = Index
            CurrentState = self.States[self.CurrentState]
            Args = [*CurrentState["Data"], True]
            threading.Thread(None, CurrentState["Function"], None, Args).start()
