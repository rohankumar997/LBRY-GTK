################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import gi, math, os, json, shutil, threading

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk, GLib, GdkPixbuf

from flbry import connect, wallet, settings

from Source import Places, Popup, SettingsUpdate
from Source.Icons import LogoBig

with open(Places.JsonDir + "DefaultSettings.json", "r") as File:
    DefaultSettings = json.load(File)

with open(Places.JsonDir + "DefaultSession.json", "r") as File:
    DefaultSession = json.load(File)


class Startup:
    Started = False

    def __init__(
        self, Window, Balance, Signing, ExitQueue, Stater, Title, KillAll
    ):
        Builder = Gtk.Builder.new_from_file(Places.GladeDir + "Startup.glade")
        Builder.connect_signals(self)
        self.Window, self.Balance, self.Signing = Window, Balance, Signing
        self.ExitQueue, self.Stater, self.Title = ExitQueue, Stater, Title
        self.KillAll, self.Startup = KillAll, Builder.get_object("Startup")
        self.StartupProgress = Builder.get_object("StartupProgress")
        self.StatusMenu = Builder.get_object("StatusMenu")
        self.ShowHide = Builder.get_object("ShowHide")

        self.Status = {
            "blob_manager": Builder.get_object("BlobManager"),
            "database": Builder.get_object("Database"),
            "dht": Builder.get_object("DHT"),
            "exchange_rate_manager": Builder.get_object("ExchangeRateManager"),
            "file_manager": Builder.get_object("FileManager"),
            "hash_announcer": Builder.get_object("HashAnnouncer"),
            "libtorrent_component": Builder.get_object("LibtorrentComponent"),
            "peer_protocol_server": Builder.get_object("PeerProtocolServer"),
            "upnp": Builder.get_object("UPNP"),
            "wallet": Builder.get_object("Wallet"),
            "wallet_server_payments": Builder.get_object("WalletServerPayment"),
            "background_downloader": Builder.get_object("BackgroundDownloader"),
            "disk_space": Builder.get_object("DiskSpace"),
            "LBRY-GTK": Builder.get_object("LBRY-GTK"),
            "tracker_announcer_component": Builder.get_object(
                "TrackerAnnouncerComponent"
            ),
        }

        for Dir in [Places.CacheDir, Places.ConfigDir]:
            try:
                os.makedirs(Dir)
            except:
                pass

        try:
            with open(Places.ConfigDir + "Session.json", "r") as File:
                json.load(File)
        except:
            with open(Places.ConfigDir + "Session.json", "w") as File:
                json.dump(
                    DefaultSession, File, indent=None, separators=(",\n", ": ")
                )

        try:
            with open(Places.CacheDir + "Notification.json", "r") as File:
                json.load(File)
        except:
            with open(Places.CacheDir + "Notification.json", "w") as File:
                json.dump({"LastNotification": -1}, File)

    def StartupHelper(self):
        self.Started = False
        self.Replace("Startup")
        self.Title.set_text("Status")

        shutil.rmtree(Places.TmpDir, ignore_errors=True)
        os.makedirs(Places.TmpDir)

        with open(Places.ConfigDir + "Session.json", "r") as File:
            Session = json.load(File)

        for Setting in DefaultSession:
            if not (
                Setting in Session
                and isinstance(Session[Setting], type(DefaultSession[Setting]))
            ):
                Session[Setting] = DefaultSession[Setting]

        for Setting in ["Binary", "Server"]:
            if Session["New" + Setting] != "":
                Session[Setting] = Session["New" + Setting]

        with open(Places.ConfigDir + "Session.json", "w") as File:
            json.dump(Session, File)

        GLib.idle_add(self.WindowSettings, Session)

        if Session["Start"]:
            Connected = connect.start(
                self.UpdateStartupProgress,
                self.ExitQueue,
                Session["Binary"],
                Session["Timeout"],
                server=Session["Server"],
            )
            Message = "LBRYNet could not be started."
        else:
            Connected = connect.check_timeout(
                self.UpdateStartupProgress,
                Session["Timeout"],
                server=Session["Server"],
            )
            Message = "LBRYNet is not running."

        if not Connected:
            Popup.Message(Message, self.Window)
            return

        LBRYSettings = settings.get(server=Session["Server"])
        if isinstance(LBRYSettings, str):
            Popup.Error(LBRYSettings, self.Window)
            return
        if not "shared" in LBRYSettings["preferences"]:
            LBRYSettings["preferences"]["shared"] = {}
        if not "local" in LBRYSettings["preferences"]:
            LBRYSettings["preferences"]["local"] = {}

        Shared = LBRYSettings["preferences"]["shared"]
        Local = LBRYSettings["preferences"]["local"]

        if not "value" in Shared:
            Shared["value"] = {}

        if not "value" in Local:
            Local["value"] = {}

        Shared, Local = Shared["value"], Local["value"]

        for Setting in ["subscriptions", "following", "tags"]:
            if not Setting in Shared:
                Shared[Setting] = []
        for Setting in ["settings", "LBRY-GTK"]:
            if not Setting in Shared:
                Shared[Setting] = {}
        if not "tags" in Local:
            Local["tags"] = []

        if not "show_mature" in Shared["settings"]:
            Shared["settings"]["show_mature"] = False

        LBRYGTKSettings = Shared["LBRY-GTK"]

        for Setting in DefaultSettings:
            if not (
                Setting in LBRYGTKSettings
                and isinstance(
                    LBRYGTKSettings[Setting], type(DefaultSettings[Setting])
                )
            ):
                LBRYGTKSettings[Setting] = DefaultSettings[Setting]

        settings.set(LBRYSettings, server=Session["Server"])

        GLib.idle_add(self.FinishStartup, Session, LBRYGTKSettings)

        self.Started = True

        if self.Stater.CurrentState == -1:
            self.Stater.Import(
                LBRYGTKSettings["HomeFunction"], LBRYGTKSettings["HomeData"]
            )

        GLib.idle_add(self.UpdateBalance, False)
        GLib.idle_add(
            self.CommentServerUpdate, Session, LBRYSettings, LBRYGTKSettings
        )
        GLib.timeout_add(60 * 1000, self.UpdateBalance, True)

    def CommentServerUpdate(self, Session, LBRYSettings, LBRYGTKSettings):
        if LBRYGTKSettings[
            "CommentServer"
        ] == "https://comments.odysee.com/api/v2" and not os.path.exists(
            Places.CacheDir + "CommentServerUpdate"
        ):
            Dialog = Gtk.MessageDialog(
                self.Window, buttons=Gtk.ButtonsType.OK_CANCEL
            )
            CheckButton = Gtk.CheckButton.new_with_label("Never Remind")
            ActionArea = Dialog.get_action_area()
            ActionArea.add(CheckButton)
            ActionArea.reorder_child(CheckButton, 0)
            Dialog.props.text = "LBRY-GTK has detected that you are using an older Comment Server.\nWould you like to update it?"
            Dialog.show_all()
            Response = Dialog.run()
            Dialog.destroy()
            if Response == Gtk.ResponseType.OK:
                Args = [Session, LBRYSettings, LBRYGTKSettings]
                threading.Thread(
                    None, self.CommentServerThread, None, Args
                ).start()
            if CheckButton.get_active():
                with open(Places.CacheDir + "CommentServerUpdate", "w") as File:
                    pass

    def CommentServerThread(self, Session, LBRYSettings, LBRYGTKSettings):
        LBRYGTKSettings["CommentServer"] = DefaultSettings["CommentServer"]
        settings.set(LBRYSettings, server=Session["Server"])

    def ShowStatusThread(self, State=False):
        self.KillAll()
        if not State:
            self.Stater.Save(self.ShowStatusThread, [], "Status")
        with open(Places.ConfigDir + "Session.json", "r") as File:
            Session = json.load(File)
        Status = connect.status(server=Session["Server"])
        Status["LBRY-GTK"] = self.Started
        GLib.idle_add(self.ShowStatusUpdate, Status)

    def ShowStatusUpdate(self, Status):
        self.Replace("Startup")
        self.Title.set_text("Status")
        self.UpdateStartupProgressHelper(Status)

    def FinishStartup(self, Session, LBRYGTKSettings):
        if LBRYGTKSettings["AuthToken"] == "":
            self.Signing.set_label("Sign In")
        self.Status["LBRY-GTK"].set_active(True)
        self.StartupProgress.set_fraction(1)
        self.StartupProgress.show_all()

        # Enable Inbox
        SettingsUpdate.ChangeMeta(self.Window, Session["EnableMetaService"])

    def WindowSettings(self, Session):

        # Minimize window
        if Session["Minimized"]:
            self.Window.iconify()
        else:
            self.Window.show_all()

        # Creating tray icon with its menu
        if Session["Tray"]:
            self.StatusIcon = Gtk.StatusIcon.new_from_pixbuf(LogoBig)
            self.StatusIcon.set_title("LBRY-GTK")
            self.StatusIcon.connect("activate", self.on_ShowHide_activate)
            self.StatusIcon.connect("popup-menu", self.on_StatusIcon_popup_menu)
            self.StatusIcon.set_visible(True)
            if Session["TrayMinimized"]:
                self.ShowHide.set_label("Show")
                self.Window.hide()

        # Changing menu type
        SettingsUpdate.ChangeMenu(
            self.Window, Session["MenuType"], Session["MenuIcon"]
        )

        # Set size of window
        if Session["WindowSize"]:
            self.Window.resize(Session["WindowWidth"], Session["WindowHeight"])

    def on_Quit_activate(self, Widget):
        self.Window.destroy()

    def UpdateStartupProgressHelper(self, Status):
        Started = 0
        for CheckBox in Status:
            self.Status[CheckBox].set_active(False)
            if Status[CheckBox]:
                Started += 1
                self.Status[CheckBox].set_active(True)
                self.Status[CheckBox].show_all()

        self.StartupProgress.set_fraction(Started / len(self.Status))
        self.StartupProgress.show_all()

    def UpdateStartupProgress(self, Status):
        GLib.idle_add(self.UpdateStartupProgressHelper, Status)

    def NoToggleBefore(self, Widget):
        Widget.set_name(str(Widget.get_active()))

    def NoToggleAfter(self, Widget):
        Widget.set_active(Widget.get_name() == "True")

    def on_ShowHide_activate(self, Widget):
        if self.Window.get_visible():
            self.ShowHide.set_label("Show")
            self.Window.hide()
        else:
            self.ShowHide.set_label("Hide")
            self.Window.show_all()

    def on_StatusIcon_popup_menu(self, Widget, Button, Time):
        self.StatusMenu.popup(None, None, None, None, Button, Time)

    def UpdateBalance(self, ReturnValue):
        with open(Places.ConfigDir + "Session.json", "r") as File:
            Session = json.load(File)
        GotBalance = wallet.balance(server=Session["Server"])
        if isinstance(GotBalance, str):
            Popup.Error(GotBalance, self.Window)
        else:
            Balance = float(GotBalance["total"])
            self.Balance.set_label(str(math.trunc(Balance)))
            self.Balance.set_tooltip_text(str(Balance))
            self.Balance.show_all()
        return ReturnValue
