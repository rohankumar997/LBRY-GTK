################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import gi, threading, time, os, json, shutil

gi.require_version("Gtk", "3.0")
from gi.repository import GLib, Gdk, Gtk

from flbry import following, main, settings, error, channel, collection, support

from Source import Places, Settings, Popup
from Source.Open import Open
from Source.Channels import Channels
from Source.Select import Select
from Source.Icons import LBCLabel

Types = [Gdk.BUTTON_PRIMARY, Gdk.BUTTON_MIDDLE]


class PublicationControl:
    def __init__(self, *args):
        (
            self.Window,
            self.Tagger,
            self.GetPublication,
            self.Documenter,
            self.Links,
            self.Stater,
            self.AddPage,
        ) = args
        Builder = Gtk.Builder.new_from_file(
            Places.GladeDir + "PublicationControl.glade"
        )
        Builder.connect_signals(self)
        self.PublicationControl = Builder.get_object("PublicationControl")
        self.TypeDependent = Builder.get_object("TypeDependent")
        self.Regular = Builder.get_object("Regular")
        self.UnFollow = Builder.get_object("UnFollow")
        self.Download = Builder.get_object("Download")
        self.Play = Builder.get_object("Play")
        self.UnBlock = Builder.get_object("UnBlock")

        self.Buttons = self.TypeDependent.get_children()
        self.Buttons.extend(self.Regular.get_children())

        self.Selector = Select(self.Buttons, [], [], [], [])

    def GetBlock(self):
        LBRYSettings = Settings.Get()
        if isinstance(LBRYSettings, str):
            Popup.Error(LBRYSettings, self.Window)
            return
        Shared = LBRYSettings["preferences"]["shared"]["value"]
        Block = self.Channel in Shared["LBRY-GTK"]["NotChannels"]
        GLib.idle_add(self.SetBlock, Block)

    def SetBlock(self, Block):
        if Block:
            self.UnBlock.set_label("Unblock")
        else:
            self.UnBlock.set_label("Block")

    def GetFollow(self):
        with open(Places.ConfigDir + "Session.json", "r") as File:
            Session = json.load(File)
        IfFollow = following.if_follow(self.Channel, server=Session["Server"])
        GLib.idle_add(self.SetFollow, IfFollow)

    def SetFollow(self, Follow):
        if Follow:
            self.UnFollow.set_label("Unfollow")
        else:
            self.UnFollow.set_label("Follow")

    def ShowPublicationControl(self, *args):
        self.Selector.Unselect(True)
        self.Data, self.Title = args
        self.ValueType = self.Data["value_type"]
        self.Url = self.Data["canonical_url"]
        self.ChannelShort = self.Data["short_url"]

        AddBuy = ""
        if "fee" in self.Data["value"]:
            AddBuy = "Buy/"
        self.Download.set_label(AddBuy + "Download")
        self.Play.set_label(AddBuy + "Play")

        try:
            self.StreamType = self.Data["value"]["stream_type"]
        except:
            self.StreamType = ""

        try:
            self.Channel = self.Data["signing_channel"]["permanent_url"]
        except:
            self.Channel = self.Data["permanent_url"]

        for Widget in self.TypeDependent.get_children():
            Show = self.ValueType in Widget.get_style_context().list_classes()
            Widget.set_visible(Show)
        threading.Thread(None, self.GetFollow).start()
        threading.Thread(None, self.GetBlock).start()

    def on_UnBlock_button_press_event(self, Widget, Event):
        if not Widget.get_realized():
            return
        threading.Thread(None, self.UnBlockHelper).start()

    def UnBlockHelper(self):
        LBRYSettings = Settings.Get()
        if isinstance(LBRYSettings, str):
            Popup.Error(LBRYSettings, self.Window)
            return
        Shared = LBRYSettings["preferences"]["shared"]["value"]
        Block = self.Channel in Shared["LBRY-GTK"]["NotChannels"]
        if self.UnBlock.get_label() == "Unblock":
            if Block:
                Shared["LBRY-GTK"]["NotChannels"].remove(self.Channel)
            Settings.Set(LBRYSettings)
            GLib.idle_add(self.SetBlock, False)
        else:
            if not Block:
                Shared["LBRY-GTK"]["NotChannels"].append(self.Channel)
            Settings.Set(LBRYSettings)
            GLib.idle_add(self.SetBlock, True)

    def on_UnFollow_button_press_event(self, Widget, Event):
        if not Widget.get_realized():
            return
        threading.Thread(None, self.UnFollowHelper).start()

    def UnFollowHelper(self):
        with open(Places.ConfigDir + "Session.json", "r") as File:
            Session = json.load(File)
        if self.UnFollow.get_label() == "Unfollow":
            if following.unfollow_channel(
                self.Channel, server=Session["Server"]
            ):
                GLib.idle_add(self.SetFollow, False)
        else:
            if following.follow_channel(self.Channel, server=Session["Server"]):
                GLib.idle_add(self.SetFollow, True)

    def on_Channel_button_press_event(self, Widget, Event):
        args = [self.Channel]
        if Event.button == Types[0]:
            threading.Thread(None, self.GetPublication, None, args).start()
        elif Event.button == Types[1]:
            self.AddPage(".", "", self.Stater.Export(self.GetPublication, args))

    def Confirmation(self):
        Dialog = Gtk.MessageDialog(
            self.Window, buttons=Gtk.ButtonsType.OK_CANCEL
        )
        Amount = self.Data["value"]["fee"]["amount"]
        Currency = self.Data["value"]["fee"]["currency"]
        Dialog.props.text = (
            "Are you sure you want to resolve this publication? (%s %s)"
            % (Amount, Currency)
        )
        Response = Dialog.run()
        Dialog.destroy()
        return Response == Gtk.ResponseType.OK

    def on_Play_button_press_event(self, Widget, Event):
        if not Widget.get_label().startswith("Buy") or self.Confirmation():
            threading.Thread(
                None, self.ResolveHelper, None, [Event.button]
            ).start()

    def on_Download_button_press_event(self, Widget, Event):
        if not Widget.get_realized():
            return
        if not Widget.get_label().startswith("Buy") or self.Confirmation():
            threading.Thread(None, self.ResolveHelper).start()

    def ResolveHelper(self, Button=False):
        LBRYSettings = Settings.Get()
        if isinstance(LBRYSettings, str):
            Popup.Error(LBRYSettings, self.Window)
            return
        Directory = ""
        if Button and not LBRYSettings["settings"]["save_files"]:
            Directory = Places.TmpDir
        JsonData = main.get(
            self.Url,
            "",
            Directory,
            save_file=True,
            server=LBRYSettings["Session"]["Server"],
        )

        if isinstance(JsonData, str):
            Popup.Error(JsonData, self.Window)
            return

        Path = JsonData["download_path"]

        Args = [
            Button,
            LBRYSettings["settings"]["save_files"],
            LBRYSettings["settings"]["download_dir"],
            JsonData["download_directory"],
            JsonData["download_path"],
            JsonData["stream_name"],
        ]

        threading.Thread(None, self.MoveThread, None, Args, daemon=True).start()

        if Button:
            Shared = LBRYSettings["preferences"]["shared"]["value"]
            Command = Shared["LBRY-GTK"]["PlayCommand"]
            Setting = self.StreamType.capitalize() + "Command"
            try:
                Command = Shared["LBRY-GTK"][Setting]
            except:
                pass
            if self.StreamType == "document" and Command == "LBRY-GTK-Document":
                GLib.idle_add(self.DocumentUpdate, Path, Button)
            else:
                try:
                    Open(Path, Command)
                except Exception as e:
                    Popup.Error(error.error(e), self.Window)

    def MoveThread(
        self, Button, SaveFiles, Location, DownloadDirectory, Path, Name
    ):
        if (not Button) or (SaveFiles and Location != DownloadDirectory):
            Counter, Size = 0, 0
            while True:
                time.sleep(0.1)
                try:
                    NewSize = os.path.getsize(Path)
                    if Size == NewSize:
                        Counter += 1
                    else:
                        Size = NewSize
                        Counter = 0
                except:
                    Counter = 0
                if Counter == 10:
                    break
            if Location[-1] != os.sep:
                Location += os.sep
            shutil.move(Path, Location + Name)

    def DocumentUpdate(self, Path, Button):
        Args = [Path, self.Title]
        if Button == Types[0]:
            threading.Thread(None, self.Documenter.Display, None, Args).start()
        elif Button == Types[1]:
            self.AddPage(
                ".", "", self.Stater.Export(self.Documenter.Display, Args)
            )

    def on_Content_button_press_event(self, Widget, Event):
        Terms = ["Search", "Content"]
        if self.ValueType == "collection":
            NewTitle, LBRYSettings = "Collection: " + self.Title, Settings.Get()
            if isinstance(LBRYSettings, str):
                Popup.Error(self.Window, LBRYSettings)
                return []
            ClaimIds, Page = [], 1
            while True:
                NewClaims = collection.resolve(
                    page=Page,
                    url=self.Url,
                    server=LBRYSettings["Session"]["Server"],
                )
                Page += 1
                for Claim in NewClaims:
                    ClaimIds.append(Claim[4])
                if len(NewClaims) != 5:
                    break
            QueryOptions = {"claim_ids": ClaimIds}
        elif self.ValueType == "channel":
            NewTitle = "Publications: " + self.Title
            QueryOptions = {"channel": self.Channel}
        Terms.extend([NewTitle, QueryOptions])
        if Event.button == Types[0]:
            threading.Thread(None, self.ButtonThread, None, Terms).start()
        elif Event.button == Types[1]:
            self.AddPage(".", "", self.Stater.Export(self.ButtonThread, Terms))

    def on_Related_button_press_event(self, Widget, Event):
        Terms = ["Search", "Content"]
        Search = self.Data["name"].replace("-", " ")
        if Search[0] == "@":
            Search = ""
        Tags, NewTitle = self.Tagger.Tags, "Related: " + self.Title
        if self.Tagger.Tags[0] == "I told you":
            Tags = []
        QueryOptions = {
            "text": Search,
            "any_tags": Tags,
            "order_by": [],
        }
        Terms.extend([NewTitle, QueryOptions])
        if Event.button == Types[0]:
            threading.Thread(None, self.ButtonThread, None, Terms).start()
        elif Event.button == Types[1]:
            self.AddPage(".", "", self.Stater.Export(self.ButtonThread, Terms))

    def on_Link_button_press_event(self, Widget, Event):
        if not Widget.get_realized():
            return
        threading.Thread(None, self.LinkThread).start()

    def LinkThread(self):
        LBRYSettings = Settings.Get()
        if isinstance(LBRYSettings, str):
            Popup.Error(LBRYSettings, self.Window)
            return
        Shared = LBRYSettings["preferences"]["shared"]["value"]
        LinkCommand = Shared["LBRY-GTK"]["LinkCommand"]
        LinkSite = Shared["LBRY-GTK"]["LinkSite"]
        Link = self.Links[LinkSite]
        try:
            Open(Link, LinkCommand)
        except Exception as e:
            Popup.Error(error.error(e), self.Window)

    def on_Share_button_press_event(self, Widget, Event):
        if not Widget.get_realized():
            return
        threading.Thread(None, self.ShareThread).start()

    def ShareThread(self):
        LBRYSettings = Settings.Get()
        if isinstance(LBRYSettings, str):
            Popup.Error(LBRYSettings, self.Window)
            return
        Shared = LBRYSettings["preferences"]["shared"]["value"]
        ShareSite = Shared["LBRY-GTK"]["ShareSite"]
        Link = self.Links[ShareSite]
        Gtk.Clipboard.set_text(
            Gtk.Clipboard.get(Gdk.SELECTION_CLIPBOARD), Link, -1
        )
        Message = "Publication link: %s copied to clipboard." % Link
        Popup.Message(Message, self.Window)

    def on_RSS_button_press_event(self, Widget, Event):
        if not Widget.get_realized():
            return
        LBRYSettings = Settings.Get()
        if isinstance(LBRYSettings, str):
            Popup.Error(LBRYSettings, self.Window)
            return
        LBRYSettings = LBRYSettings["preferences"]["shared"]["value"]

        RSS = self.ChannelShort.replace("#", ":").split("lbry://", 1)[1]

        if LBRYSettings["LBRY-GTK"]["RSSSite"] == 0:
            Link = "https://odysee.com/$/rss/" + RSS
        elif LBRYSettings["LBRY-GTK"]["RSSSite"] == 1:
            Link = "https://lbry.bcow.xyz/" + RSS + "/rss"
        else:
            Link = "https://lbryfeed.melroy.org/channel/" + RSS

        Gtk.Clipboard.set_text(
            Gtk.Clipboard.get(Gdk.SELECTION_CLIPBOARD), Link, -1
        )

        Popup.Message("RSS link: %s copied to clipboard." % Link, self.Window)

    def CreateInputWindow(self, Title, Session):
        Adjustment = Gtk.Adjustment.new(0, 0, 1000000000000, 1, 10, 0)
        SpinButton = Gtk.SpinButton.new(Adjustment, 0, 8)
        Dialog = Gtk.MessageDialog(
            self.Window,
            Gtk.DialogFlags.DESTROY_WITH_PARENT,
            buttons=Gtk.ButtonsType.OK_CANCEL,
        )
        Dialog.set_title(Title)
        ContentArea = Dialog.get_content_area()

        LBRYSettings = Settings.Get()
        if isinstance(LBRYSettings, str):
            Popup.Error(LBRYSettings, self.Window)
            return [0]
        LBRYSettings = LBRYSettings["preferences"]["shared"]["value"][
            "LBRY-GTK"
        ]

        ChannelList = channel.channel_list(server=Session["Server"])
        Channelser = Channels(False, self.Window, ChannelList, LBRYSettings)
        ContentArea.add(Gtk.Label.new("Channel"))
        ToAdd = Channelser.Channels.get_parent()
        ToAdd.get_children()[1].hide()
        ContentArea.add(ToAdd)
        ContentArea.add(Gtk.Label.new("Amount"))
        Box = Gtk.Box.new(Gtk.Orientation.HORIZONTAL, 0)
        Box.add(SpinButton)
        Image = Gtk.Image.new_from_pixbuf(LBCLabel)
        Box.add(Image)
        ContentArea.add(Box)

        Dialog.show_all()
        Response = Dialog.run()
        Dialog.destroy()
        if Response == Gtk.ResponseType.OK:
            return [SpinButton.get_value(), Channelser.Get()]
        return [0]

    def PayUp(self, Name, BoolSet):
        with open(Places.ConfigDir + "Session.json", "r") as File:
            Session = json.load(File)
        Value = self.CreateInputWindow(Name, Session)
        if Value[0] != 0:
            JsonData = support.create(
                self.Data["claim_id"],
                Value[0],
                BoolSet,
                Value[1][0],
                Value[1][5],
                Session["Server"],
            )
            if isinstance(JsonData, str):
                Popup.Error(JsonData, self.Window)

    def on_TipBoost_button_press_event(self, Widget, Event):
        if Widget.get_realized():
            Name = Widget.get_label()
            self.PayUp(Name, Name == "Tip")
            return True

    def NoToggleBefore(self, Widget):
        Widget.set_name(str(Widget.get_active()))

    def NoToggleAfter(self, Widget):
        Widget.set_active(Widget.get_name() == "True")

    # Not Yet Implemented

    def on_Repost_button_press_event(self, Widget, Event):
        This = "is not working yet"
