################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import gi, threading, json

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GLib

from flbry import channel, main

from Source import Places
from Source.Tag import Tag


class Language:
    def __init__(self):
        Builder = Gtk.Builder.new_from_file(Places.GladeDir + "Language.glade")
        Builder.connect_signals(self)
        self.Language = Builder.get_object("Language")
        self.Combo = Builder.get_object("Combo")
        self.Entry = Builder.get_object("Entry")
        self.Model = Builder.get_object("Model")

        self.Tagger = Tag(True)
        self.Tagger.on_Add_clicked = self.on_Add_clicked
        self.Language.pack_start(self.Tagger.Tag, True, True, 0)
        self.Tagger.Entry.hide()

        self.RemoveAll = self.Tagger.RemoveAll

        with open(Places.JsonDir + "Languages.json") as Languages:
            self.Languages = json.load(Languages)

        for Language in self.Languages:
            LanguageText = Language[0] + "/" + Language[1]
            self.Combo.append(None, LanguageText)
            self.Model.append([LanguageText])

    def Helper(self, Button):
        if Button == 1:
            self.Combo.popup()
        else:
            self.Tagger.KeybindHelper(Button)

    def on_Add_clicked(self, Widget=""):
        OldLength = len(self.Tagger.Tags)
        self.Tagger.Add(self.Tagger.Entry.get_text())
        if OldLength != len(self.Tagger.Tags):
            self.Entry.set_text("")

    def on_Entry_key_press_event(self, Widget, Event):
        OldLength = len(self.Tagger.Tags)
        self.Tagger.on_Entry_key_press_event(Widget, Event)
        if OldLength != len(self.Tagger.Tags):
            self.Entry.set_text("")

    def on_Entry_changed(self, Widget):
        self.Tagger.Entry.set_text("")
        NewLanguage = Widget.get_text()
        ID = 0
        for Language in self.Languages:
            if NewLanguage == (Language[0] + "/" + Language[1]):
                self.Combo.set_active(ID)
                self.Tagger.Entry.set_text(NewLanguage)
                break
            ID += 1

    def Append(self, List):
        for Item in List:
            for Language in self.Languages:
                if Language[0] == Item:
                    self.Tagger.Add(Language[0] + "/" + Language[1])
                    break
