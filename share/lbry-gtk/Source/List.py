################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import gi, threading, json, queue, copy, time

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GLib, GdkPixbuf

from flbry import search, wallet, list_files, meta

from Source import Image, Places, Settings, ListGridUtil, Popup
from Source.Box import Box

ClaimTypes = {"stream": 1, "channel": 2, "repost": 3, "collection": 4}

EmptyPixbuf = GdkPixbuf.Pixbuf.new_from_data(
    [0, 0, 0, 0], GdkPixbuf.Colorspace.RGB, True, 8, 1, 1, 1, None, None
)


class List:
    ListPage, Loaded, Started, RowCache = 0, 1, False, []

    def __init__(self, *args):
        (
            self.NoContentNotice,
            self.View,
            self.Window,
            self.Publicationer,
            self.Stores,
            self.WalletSpaceParts,
            self.MainSpace,
            self.Stater,
            self.Grid,
            self.Spaces,
            self.Text,
            self.Orderer,
            self.Channel,
            self.ClaimType,
            self.TaggersDict,
            self.DateTimer,
            self.Inequality,
            self.Advanced,
            self.DateType,
            self.Title,
            self.AddPage,
        ) = args

        Builder = Gtk.Builder.new_from_file(Places.GladeDir + "Box.glade")
        Height = Builder.get_object("Type").get_preferred_height()
        self.TextHeight = Height.minimum_height
        Height = Builder.get_object("IsTip").get_preferred_height()
        self.CheckHeight = Height.minimum_height

        self.ListFunctions = {
            "Search": [search.simple],
            "Wallet": [
                ListGridUtil.BalanceHelper,
                wallet.history,
                self.WalletSpaceParts,
                self.Window,
            ],
            "File": [list_files.downloaded],
            "Inbox": [ListGridUtil.InboxHelper, meta.list],
        }
        self.Threads = []

    def UpdateMainSpace(self, LBRYSettings, Increase=0):
        self.Loaded += Increase
        for Thread in self.Threads:
            Thread[1].get()
        self.ListThread(LBRYSettings, *self.ListFunctions[self.ListButton])

    def Empty(self, Queue):
        self.Stores["Row"].clear()
        self.Grid.forall(Gtk.Widget.destroy)
        Queue.put(True)

    def Exit(self, ThreadQueue, UpdateMainSpaceQueue):
        self.Threads.remove([ThreadQueue, UpdateMainSpaceQueue])
        UpdateMainSpaceQueue.put(True)
        self.Started = True

    def ListThread(self, LBRYSettings, ListFunction, *args):
        ThreadQueue = queue.Queue()
        UpdateMainSpaceQueue = queue.Queue()
        self.Threads.append([ThreadQueue, UpdateMainSpaceQueue])
        Server = LBRYSettings["Session"]["Server"]
        LBRYGTKSettings = LBRYSettings["preferences"]["shared"]["value"][
            "LBRY-GTK"
        ]
        for Index in range(self.ListPage, self.Loaded):
            SingleMessage = True
            while True:
                # Data request
                Data = ListFunction(
                    *args,
                    **self.Data,
                    page_size=max(1, int(LBRYGTKSettings["ContentPerLoading"])),
                    page=Index + 1,
                    server=Server,
                )
                # Returned number tells the page number change (lbrynet)
                PlacedQueue = queue.Queue()
                GLib.idle_add(
                    self.PlaceData,
                    LBRYGTKSettings,
                    ThreadQueue,
                    Data,
                    PlacedQueue,
                )
                PlacedReturn = PlacedQueue.get()

                # Try to reget data, if an error occured
                if PlacedReturn != -1:
                    break
                elif SingleMessage:
                    Popup.Message(
                        "LBRYNet is experiencing a problem, the client is retrying to load the data.",
                        self.Window,
                    )
                    Popup.Error(PlacedQueue.get(), self.Window)
                    SingleMessage = False
                try:
                    ThreadQueue.get(block=False)
                    break
                except:
                    pass

            # If PlacedReturn is -2 then page is being changed
            if PlacedReturn != -2:
                self.ListPage = Index + PlacedReturn
            # If placing data wasn't succesful - break execution
            if PlacedReturn != 1:
                break
        self.Exit(ThreadQueue, UpdateMainSpaceQueue)

    def PlaceData(self, LBRYGTKSettings, ThreadQueue, Data, Queue):
        Space = self.Spaces[self.List]
        if len(Data) == 0:
            if self.ListPage == 0 and not self.NoContentNotice.get_parent():
                Space.add(self.NoContentNotice)
                self.MainSpace.show_all()
            Queue.put(0)
            return
        if isinstance(Data, str):
            Queue.put(-1)
            Queue.put(Data)
            return
        # Making either grid or list for items
        if self.ListPage == 0:
            Item = self.Grid
            if self.ListDisplay == 0:
                Item = self.View
                for Index in range(1, 4):
                    Item.get_column(Index).set_visible(self.List == "Wallet")
            Space.add(Item)
        # Data is placed to grid/list here
        for Row in Data:
            try:
                ThreadQueue.get(block=False)
                Queue.put(-2)
                return
            except:
                pass

            # Checks if already exists on the page
            if (
                Row[-1] in self.RowCache
                and self.List != "Wallet"
                and self.ListButton != "Inbox"
            ):
                continue
            else:
                self.RowCache.append(Row[-1])

            EmptyCopy = EmptyPixbuf.scale_simple(
                int(LBRYGTKSettings["ThumbnailWidth"]),
                int(LBRYGTKSettings["ThumbnailHeight"]),
                GdkPixbuf.InterpType.NEAREST,
            )

            Args = [Row, EmptyCopy, self.MainSpace, self.List]
            if self.ListDisplay == 0:
                ListGridUtil.ListUpdate(*Args, self.Stores["Row"])
            else:
                ListGridUtil.GridUpdate(
                    *Args,
                    LBRYGTKSettings,
                    self.Publicationer,
                    self.Grid,
                    self.AddPage,
                    self.Stater,
                )
        Queue.put(1)

    def GenericLoaded(self, LBRYSettings):
        LBRYGTKSettings = LBRYSettings["preferences"]["shared"]["value"][
            "LBRY-GTK"
        ]
        ContentQueue = queue.Queue()
        ContentFunction = ListGridUtil.GridContent
        if self.ListDisplay == 0:
            ContentFunction = ListGridUtil.ListContent
        GLib.idle_add(
            ContentFunction,
            int(LBRYGTKSettings["ThumbnailHeight"]),
            int(LBRYGTKSettings["ThumbnailWidth"]),
            self.MainSpace,
            ContentQueue,
            LBRYGTKSettings,
            self.List,
            self.CheckHeight,
            self.TextHeight,
        )
        Content = ContentQueue.get()
        Content = (
            Content // max(1, int(LBRYGTKSettings["ContentPerLoading"])) + 1
        )
        if self.Loaded < Content:
            self.Loaded = Content
        if self.ListPage < self.Loaded:
            self.UpdateMainSpace(LBRYSettings)

    def ButtonThread(self, ButtonName, ListName, Title, Data={}, State=False):
        self.Publicationer.Commenter.KillAll()
        EmptyQueue = queue.Queue()
        GLib.idle_add(self.Empty, EmptyQueue)
        EmptyQueue.get()

        self.RowCache.clear()
        self.Started, self.Loaded = False, 0
        for Thread in self.Threads:
            Thread[0].put(True)
        while len(self.Threads) != 0:
            time.sleep(0.01)
        LBRYSettings = Settings.Get()
        if isinstance(LBRYSettings, str):
            Popup.Error(LBRYSettings, self.Window)
            return
        LBRYGTKSettings = LBRYSettings["preferences"]["shared"]["value"][
            "LBRY-GTK"
        ]
        self.ListDisplay = LBRYGTKSettings["ListDisplay"]
        self.Data = Data
        Args = [ButtonName, ListName, Title, Data]
        if not State:
            self.Stater.Save(self.ButtonThread, Args, Title)
        GLib.idle_add(self.ButtonUpdate, *Args, LBRYSettings)

    def ButtonUpdate(self, ButtonName, ListName, Title, Data, LBRYSettings):
        Shared = LBRYSettings["preferences"]["shared"]["value"]
        LBRYGTKSettings = Shared["LBRY-GTK"]

        # Initialization
        self.Advanced.set_expanded(True)
        self.Advanced.activate()
        self.Advanced.set_expanded(False)
        self.Orderer.Set(LBRYGTKSettings["Order"])
        self.Text.set_text("")
        self.Channel.set_text("")
        self.ClaimType.set_active(0)
        self.Inequality.set_active(0)
        self.DateType.set_active(0)
        for Index in self.TaggersDict:
            self.TaggersDict[Index][1].RemoveAll()
        self.DateTimer.DateUse.set_active(False)
        self.DateTimer.on_DateReset_button_press_event()

        # Settings for Inbox
        if ButtonName == "Inbox":
            Data["meta_server"] = LBRYGTKSettings["MetaServer"]
            Data["auth_token"] = LBRYGTKSettings["AuthToken"]
        elif "meta_server" in Data:
            del Data["meta_server"]
            del Data["auth_token"]

        NotConverts = {"not_tags": "NotTags", "not_channel_ids": "NotChannels"}

        # Settings found from Data placed
        ShowMature = Shared["settings"]["show_mature"]
        if ListName == "Content":
            if "order_by" in Data:
                if Data["order_by"] == []:
                    self.Orderer.Set("")
                else:
                    self.Orderer.Set(Data["order_by"][0])
            else:
                Order = self.Orderer.Get()
                if Order:
                    Data["order_by"] = [Order]
            for NotConvert in NotConverts:
                if not NotConvert in Data:
                    Data[NotConvert] = []
                if LBRYGTKSettings[NotConverts[NotConvert]] != []:
                    Data[NotConvert].extend(
                        copy.deepcopy(LBRYGTKSettings[NotConverts[NotConvert]])
                    )
            if not ShowMature:
                Data["not_tags"].append("mature")
                LBRYGTKSettings["NotTags"].append("mature")

        if "text" in Data:
            self.Text.set_text(Data["text"])
        if "channel" in Data:
            self.Channel.set_text(Data["channel"])
        if "claim_type" in Data:
            self.ClaimType.set_active(ClaimTypes[Data["claim_type"]])

        for Index in self.TaggersDict:
            if Index in Data:
                if Index in NotConverts:
                    for Tag in Data[Index]:
                        if not Tag in LBRYGTKSettings[NotConverts[Index]]:
                            self.TaggersDict[Index][1].Add(Tag)
                else:
                    self.TaggersDict[Index][1].Append(Data[Index])

        if "timestamp" in Data:
            Time = Data["timestamp"]
            if Time != "-1":
                Equality = [">=", "<=", "=", ">", "<"]
                Equal = 2
                for Mark in Equality:
                    if Time.startswith(Mark):
                        Equal = 4 - Equality.index(Mark)
                        Time = Time[len(Mark) :]
                        break
                self.DateType.set_active(2)
                self.DateTimer.DateUse.set_active(True)
                self.Inequality.set_active(Equal)
                self.DateTimer.SetTime(int(Time))

        self.ListButton = ButtonName
        self.List = ListName
        self.Title.set_text(Title)
        Children = [self.Grid, self.View, self.NoContentNotice]
        for Child in Children:
            Parent = Child.get_parent()
            if Parent:
                Parent.remove(Child)
        self.Replace(self.List + "List")
        self.ListPage = 0
        threading.Thread(
            None, self.GenericLoaded, None, [LBRYSettings], daemon=True
        ).start()
