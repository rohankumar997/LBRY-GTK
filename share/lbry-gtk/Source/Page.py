################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import gi, threading, queue, copy, datetime, copy, json

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk, GLib

from Source import ShowHideHelper, Places, Settings, ListCreate, Move, Popup
from Source import SelectUtil
from Source.Replace import Replace
from Source.ShowHide import ShowHide
from Source.Publication import Publication
from Source.NewPublication import NewPublication
from Source.List import List
from Source.State import State
from Source.DateTime import DateTime
from Source.Order import Order
from Source.Tag import Tag
from Source.StateThread import StateThread
from Source.Startup import Startup
from Source.Select import Select
from Source.Language import Language
from Source.Icons import LBCLabel

with open(Places.JsonDir + "Languages.json") as LanguageFile:
    Languages = json.load(LanguageFile)


class FakeItem:
    def is_visible(self):
        return True

    def get_realized(self):
        return True

    def get_name(self):
        return "FakeItem"

    def set_active(self, Discard):
        return


class Page:
    MainSpaceHeight, MainSpaceWidth = 0, 0
    ExitQueue = queue.Queue()

    def __init__(self, *Args):
        (
            self.Window,
            FirstPage,
            NoStartup,
            self.AddPage,
            self.Balance,
            self.Signing,
            self.CloseButton,
            Main,
        ) = Args
        Builder = Gtk.Builder.new_from_file(Places.GladeDir + "Page.glade")
        Builder.connect_signals(self)
        self.ShowHiderTexts, self.Stater = [], State()
        self.Page = Builder.get_object("Page")
        self.Title = Builder.get_object("Title")
        self.Title.set_text("")
        self.MainSpace = Builder.get_object("MainSpace")
        Spaces = {
            "Content": Builder.get_object("ContentSpace"),
            "Wallet": Builder.get_object("WalletSpace"),
            "File": Builder.get_object("FileSpace"),
            "Notification": Builder.get_object("NotificationSpace"),
        }

        self.NoContentNotice = Builder.get_object("NoContentNotice")
        self.DateTimeBox = Builder.get_object("DateTimeBox")
        self.OrderBox = Builder.get_object("OrderBox")
        self.Inequality = Builder.get_object("Inequality")
        self.Text = Builder.get_object("Text")
        self.Channel = Builder.get_object("Channel")
        self.ClaimType = Builder.get_object("ClaimType")
        self.StreamType = Builder.get_object("StreamType")
        self.DateType = Builder.get_object("DateType")
        self.DateLabel = Builder.get_object("DateLabel")
        self.DateInterval = Builder.get_object("DateInterval")
        self.DateValue = Builder.get_object("DateValue")
        self.ContentView = Builder.get_object("ContentView")
        self.Grid = Builder.get_object("Grid")
        self.ScrollDown = Builder.get_object("ScrollDown")
        self.ScrollUp = Builder.get_object("ScrollUp")
        self.MoveLeft = Builder.get_object("MoveLeft")
        self.MoveRight = Builder.get_object("MoveRight")
        self.MoveUp = Builder.get_object("MoveUp")
        self.MoveDown = Builder.get_object("MoveDown")
        self.ListOpenOnCurrent = Builder.get_object("ListOpenOnCurrent")
        self.ListOpenOnNew = Builder.get_object("ListOpenOnNew")
        self.Search = Builder.get_object("Search")
        self.SelectNext = Builder.get_object("SelectNext")
        self.SelectPrevious = Builder.get_object("SelectPrevious")
        self.Apply = Builder.get_object("Apply")
        self.DateTypeActive = Builder.get_object("DateTypeActive")
        self.DateIntervalActive = Builder.get_object("DateIntervalActive")
        self.DateValueActive = Builder.get_object("DateValueActive")
        self.InequalityActive = Builder.get_object("InequalityActive")
        self.ClaimTypeActive = Builder.get_object("ClaimTypeActive")
        self.StreamTypeActive = Builder.get_object("StreamTypeActive")
        self.Advanced = Builder.get_object("Advanced")
        self.TagSelect = Builder.get_object("TagSelect")
        self.SimpleDateBox = Builder.get_object("SimpleDateBox")
        self.WalletGrid = Builder.get_object("WalletGrid")
        for Child in self.WalletGrid.get_children():
            if isinstance(Child, Gtk.Image):
                Child.set_from_pixbuf(LBCLabel)
        self.LanguagerList = ["any_languages", "all_languages", "not_languages"]
        self.TaggerList = ["any_tags", "all_tags", "not_tags", "claim_ids"]
        self.TaggerList.extend(["channel_ids", "stream_types"])
        self.TaggerList.extend(["not_channel_ids", "any_languages"])
        self.TaggerList.extend(["all_languages", "not_languages"])
        self.TaggersDict, self.Taggers, self.Languagers = {}, [], []
        for Index in self.TaggerList:
            self.TaggersDict[Index] = [Builder.get_object(Index)]
            if Index in self.LanguagerList:
                Object = Language()
                self.Languagers.append(Object)
                self.TaggersDict[Index][0].pack_start(
                    Object.Language, True, True, 0
                )
            else:
                Object = Tag(True)
                self.Taggers.append(Object)
                self.TaggersDict[Index][0].pack_start(Object.Tag, True, True, 0)
            self.TaggersDict[Index].append(Object)

        self.TaggersDict["stream_types"][1].Entry.hide()
        self.on_StreamType_changed(self.StreamType)

        self.LanguagerList = ["any_languages", "all_languages", "not_languages"]

        self.DateTimer = DateTime()
        self.DateTimeBox.pack_start(self.DateTimer.DateTime, True, True, 0)
        self.DateTimer.DateTime.set_no_show_all(True)

        self.Orderer = Order()
        self.OrderBox.pack_start(self.Orderer.Order, True, True, 0)

        self.on_DateType_changed(self.DateType)

        self.Publicationer = Publication(
            self.ShowHiderTexts,
            self.Window,
            self.Stater,
            self.Title,
            self.MainSpace,
            self.AddPage,
        )

        self.NewPublicationer = NewPublication(self.Window, self.Title)

        self.ShowHider = ShowHide(
            [
                [
                    self.ShowHiderTexts,
                    ShowHideHelper.CommentShow,
                    ShowHideHelper.CommentHide,
                ],
            ],
            200,
        )

        ListCreate.MakeColumns(self.ContentView, self.Publicationer.DataView)

        self.Lister = List(
            self.NoContentNotice,
            self.ContentView,
            self.Window,
            self.Publicationer,
            {
                "Row": Builder.get_object("RowStore"),
                "Data": Builder.get_object("DataStore"),
            },
            [
                Builder.get_object("Total"),
                Builder.get_object("Available"),
                Builder.get_object("Reserved"),
                Builder.get_object("Claims"),
                Builder.get_object("Supports"),
                Builder.get_object("Tips"),
            ],
            self.MainSpace,
            self.Stater,
            self.Grid,
            Spaces,
            self.Text,
            self.Orderer,
            self.Channel,
            self.ClaimType,
            self.TaggersDict,
            self.DateTimer,
            self.Inequality,
            self.Advanced,
            self.DateType,
            self.Title,
            self.AddPage,
        )
        self.Publicationer.PublicationControler.ButtonThread = (
            self.Lister.ButtonThread
        )
        self.Publicationer.Tagger.ButtonThread = self.Lister.ButtonThread

        self.Startuper = Startup(
            self.Window,
            self.Balance,
            self.Signing,
            self.ExitQueue,
            self.Stater,
            self.Title,
            self.Publicationer.Commenter.KillAll,
        )

        self.Settingser = Settings.Settings(
            self.Window, self.Title, self.Stater, self.Startuper, Main
        )

        self.StateThreader = StateThread(
            self.Stater,
            self.NewPublicationer,
            self.Publicationer,
            self.Settingser,
            self.Lister,
        )

        self.Replacer = Replace(
            {
                "Publication": self.Publicationer.Publication,
                "ContentList": Spaces["Content"],
                "WalletList": Spaces["Wallet"],
                "FileList": Spaces["File"],
                "NotificationList": Spaces["Notification"],
                "NewPublication": self.NewPublicationer.NewPublication,
                "Settings": self.Settingser.Settings,
                "Startup": self.Startuper.Startup,
                "Document": self.Publicationer.Documenter.Markdowner.Document,
            },
            self.MainSpace,
        )
        self.Publicationer.Replace = self.Replacer.Replace
        self.Publicationer.Documenter.Replace = self.Replacer.Replace
        self.NewPublicationer.Replace = self.Replacer.Replace
        self.Startuper.Replace = self.Replacer.Replace
        self.Settingser.Replace = self.Replacer.Replace
        self.Lister.Replace = self.Replacer.Replace
        self.Stater.PossibleStates = {
            "Following": self.StateThreader.FollowingThread,
            "YourTags": self.StateThreader.YourTagsThread,
            "Native": self.StateThreader.NativeThread,
            "Discover": self.StateThreader.DiscoverThread,
            "Library": self.StateThreader.LibraryThread,
            "Library Search": self.StateThreader.LibrarySearchThread,
            "Collections": self.StateThreader.CollectionsThread,
            "Followed": self.StateThreader.FollowedThread,
            "Uploads": self.StateThreader.UploadsThread,
            "Channels": self.StateThreader.ChannelsThread,
            "Advanced Search": self.Lister.ButtonThread,
            "Document": self.Publicationer.Documenter.Display,
            "Publication": self.Publicationer.GetPublication,
            "NewPublication": self.StateThreader.NewPublicationThread,
            "Settings": self.StateThreader.SettingsThread,
            "Help": self.StateThreader.HelpThread,
            "Status": self.Startuper.ShowStatusThread,
            "Home": self.StateThreader.HomeThread,
            "Inbox": self.StateThreader.InboxThread,
        }
        DateTimer, Orderer, Fake = self.DateTimer, self.Orderer, FakeItem()
        Widgets = [Orderer.UseOrdering, Orderer.UseOrderingActive]
        Widgets.extend([Orderer.OrderBy, Orderer.OrderByActive])
        Widgets.extend([Orderer.Direction, Orderer.DirectionActive])
        Widgets.extend([self.DateType, self.DateTypeActive, self.DateInterval])
        Widgets.extend([self.DateIntervalActive, self.DateValue])
        Widgets.extend([self.DateValueActive, self.Inequality])
        Widgets.extend([self.InequalityActive, self.DateTimeBox])
        Widgets.extend([DateTimer.DateActive, DateTimer.Hour, DateTimer.Minute])
        Widgets.extend([DateTimer.Second, DateTimer.DateReset])
        Widgets.extend([DateTimer.DateUse, DateTimer.DateUseActive, self.Text])
        Widgets.extend([self.Channel, self.ClaimType, self.ClaimTypeActive])
        for Index in self.TaggerList:
            if Index != "stream_types":
                Widgets.append(self.TaggersDict[Index][0])
        Widgets.extend([self.StreamType, self.StreamTypeActive, self.Search])
        Widgets.extend([self.Apply, Fake])

        Daters = [DateTimer]
        Items = SelectUtil.PrepareWidgets(
            Widgets, self.Taggers, self.Languagers, Daters
        )
        Items[4][-1] = self.StreamTyperHelper
        self.Selector = Select(*Items)

        self.Startuper.Started = NoStartup

        self.Window.show_all()

        if not isinstance(FirstPage, str):
            self.Stater.Import(*FirstPage)

    def StopScroll(self, Widget, Event):
        Widget.emit_stop_by_name("scroll-event")

    def StreamTyperHelper(self, Button):
        if Button == 1:
            self.StreamType.popup()
        else:
            self.StreamTyper.KeybindHelper(Button)

    def Scroll(self, Direction=1):
        Adjustment = self.MainSpace.get_vadjustment()
        if self.Settingser.Settings.get_realized():
            Children = self.Settingser.Settings.get_children()
            Child = Children[1]
            if isinstance(Children[0], Gtk.Notebook):
                Child = Children[0]
            Page = Child.get_nth_page(Child.get_current_page())
            Adjustment = Page.get_vadjustment()
        Adjustment.set_value(
            Adjustment.get_value() + Direction * Adjustment.get_step_increment()
        )

    def on_ScrollDown_activate(self, Widget):
        self.Scroll()

    def on_ScrollUp_activate(self, Widget):
        self.Scroll(-1)

    def on_TagSelect_activate(self, Widget):
        if (
            self.Grid.get_realized()
            or self.ContentView.get_realized()
            or self.NoContentNotice.get_realized()
        ):
            if 15 < self.Selector.CurrentItem < 22:
                self.Taggers[self.Selector.CurrentItem - 16].Select()

    def GetFlowbox(self):
        if (
            self.Grid.get_realized()
            or self.ContentView.get_realized()
            or self.NoContentNotice.get_realized()
        ):
            if 15 < self.Selector.CurrentItem < 22:
                return self.Taggers[self.Selector.CurrentItem - 16].FlowBox
            elif self.Grid.get_realized():
                return self.Grid
        return False

    def on_MoveLeft_activate(self, Widget):
        FlowBox = self.GetFlowbox()
        if FlowBox:
            Move.FlowBoxLeftRight(FlowBox, -1)

    def on_MoveRight_activate(self, Widget):
        FlowBox = self.GetFlowbox()
        if FlowBox:
            Move.FlowBoxLeftRight(FlowBox)

    def on_MoveDown_activate(self, Widget):
        FlowBox = self.GetFlowbox()
        if FlowBox:
            Move.FlowBoxUpDown(FlowBox)

    def on_MoveUp_activate(self, Widget):
        FlowBox = self.GetFlowbox()
        if FlowBox:
            Move.FlowBoxUpDown(FlowBox, -1)

    def on_ListOpenOnCurrent_activate(self, Widget):
        if (
            self.Grid.get_realized()
            or self.ContentView.get_realized()
            or self.NoContentNotice.get_realized()
        ) and -1 < self.Selector.CurrentItem < len(self.Selector.Widgets) - 1:
            self.Selector.Activate(0)
        elif self.Grid.get_realized():
            try:
                Child = self.Grid.get_selected_children()[0].get_children()[0]
                Event = Gdk.Event.new(Gdk.EventType.BUTTON_PRESS)
                Event.button = Gdk.BUTTON_PRIMARY
                Event.window = Child.get_window()
                Child.event(Event)
            except:
                pass
        elif self.ContentView.get_realized():
            self.on_ContentView_button_press_event(
                self.ContentView, "", Gdk.BUTTON_PRIMARY
            )

    def on_ListOpenOnNew_activate(self, Widget):
        if (
            self.Grid.get_realized()
            or self.ContentView.get_realized()
            or self.NoContentNotice.get_realized()
        ) and -1 < self.Selector.CurrentItem < len(self.Selector.Widgets) - 1:
            self.Selector.Activate(1)
        elif self.Grid.get_realized():
            try:
                Child = self.Grid.get_selected_children()[0].get_children()[0]
                Event = Gdk.Event.new(Gdk.EventType.BUTTON_PRESS)
                Event.button = Gdk.BUTTON_MIDDLE
                Event.window = Child.get_window()
                Child.event(Event)
            except:
                pass
        elif self.ContentView.get_realized():
            self.on_ContentView_button_press_event(
                self.ContentView, "", Gdk.BUTTON_MIDDLE
            )

    def on_StreamType_changed(self, Widget):
        Entry = self.TaggersDict["stream_types"][1].Entry
        Entry.set_text(Widget.get_active_text().lower())

    def on_SimpleDate_changed(self, Widget=""):
        self.Inequality.set_active(1)
        DateValue = self.DateValue.get_active()
        if self.DateInterval.get_active() == 0:
            self.DateTimer.on_DateReset_button_press_event()
            self.DateTimer.Second.set_value(0)
            if -1 < DateValue:
                self.DateTimer.Minute.set_value(0)
            if 0 < DateValue:
                self.DateTimer.Hour.set_value(0)
            Date = datetime.date.today()
            if 1 < DateValue:
                Date -= datetime.timedelta(days=Date.weekday())
            if 2 < DateValue:
                Date -= datetime.timedelta(days=Date.day - 1)
            if 3 < DateValue:
                Date = datetime.date(Date.year, 1, 1)
            self.DateTimer.Date.select_day(Date.day)
            self.DateTimer.Date.select_month(Date.month - 1, Date.year)
        else:
            self.DateTimer.on_DateReset_button_press_event()
            if 0 == DateValue:
                self.DateTimer.Hour.spin(Gtk.SpinType.STEP_BACKWARD, 1)
            Date = datetime.date.today()
            if 1 == DateValue:
                Date -= datetime.timedelta(days=1)
            if 2 == DateValue:
                Date -= datetime.timedelta(days=7)
            if 3 == DateValue:
                if Date.month == 1:
                    Date = datetime.date(Date.year, 12, Date.day)
                else:
                    Date = datetime.date(Date.year, Date.month - 1, Date.day)
            if 4 == DateValue:
                Date = datetime.date(Date.year - 1, Date.month, Date.day)
            self.DateTimer.Date.select_day(Date.day)
            self.DateTimer.Date.select_month(Date.month - 1, Date.year)

    def on_DateType_changed(self, Widget):
        Children = self.DateTimeBox.get_children()
        self.DateTimer.DateUse.set_active(Widget.get_active() == 1)
        self.DateLabel.set_visible(Widget.get_active() != 0)
        if Widget.get_active() == 0:
            self.DateTimeBox.hide()
            self.SimpleDateBox.hide()
        elif Widget.get_active() == 1:
            self.DateTimeBox.hide()
            self.SimpleDateBox.show()
            self.on_SimpleDate_changed()
        elif Widget.get_active() == 2:
            self.Inequality.set_active(0)
            self.DateTimeBox.show()
            self.SimpleDateBox.hide()
            self.DateTimer.on_DateReset_button_press_event()

    def on_ContentView_button_press_event(self, Widget, Event, Button=""):
        if Button == "":
            Button = Event.button
        Store, Path = Widget.get_selection().get_selected_rows()
        Url = [Store[Path][-4]]
        if Url != [""]:
            if Button == Gdk.BUTTON_PRIMARY:
                threading.Thread(
                    None, self.Publicationer.GetPublication, None, Url
                ).start()
            elif Button == Gdk.BUTTON_MIDDLE:
                self.AddPage(".", "", ["Publication", Url])

    def on_MainSpace_draw(self, Widget, event):
        OldHeight, OldWidth = self.MainSpaceHeight, self.MainSpaceWidth
        self.MainSpaceHeight = self.MainSpace.get_allocated_height()
        self.MainSpaceWidth = self.MainSpace.get_allocated_width()
        Space = self.Replacer.GetSpace()
        if (
            (OldHeight < self.MainSpaceHeight or OldWidth < self.MainSpaceWidth)
            and Space.endswith("List")
            and self.Lister.Started
        ):
            LBRYSettings = Settings.Get()
            if isinstance(LBRYSettings, str):
                Popup.Error(self.Window, LBRYSettings)
                return
            threading.Thread(
                None,
                self.Lister.GenericLoaded,
                None,
                [LBRYSettings],
                daemon=True,
            ).start()

    def on_MainSpace_edge_overshot(self, Widget, Position):
        self.on_MainSpace_edge_reached(Widget, Position)

    def on_MainSpace_edge_reached(self, Widget, Position):
        Space = self.Replacer.GetSpace()
        if Space.endswith("List") and Position == Gtk.PositionType.BOTTOM:
            LBRYSettings = Settings.Get()
            if isinstance(LBRYSettings, str):
                Popup.Error(self.Window, LBRYSettings)
                return
            threading.Thread(
                None,
                self.Lister.UpdateMainSpace,
                None,
                [LBRYSettings, 1],
                daemon=True,
            ).start()

    def on_Search_button_press_event(self, Widget, Event):
        if Event.button == Gdk.BUTTON_PRIMARY:
            threading.Thread(
                None, self.StateThreader.LibrarySearchThread
            ).start()
        elif Event.button == Gdk.BUTTON_MIDDLE:
            self.AddPage(".", "", ["Library Search", []])

    def on_Apply_button_press_event(self, Widget, Event):
        LBRYSettings = Settings.Get()
        if isinstance(LBRYSettings, str):
            Popup.Error(self.Window, LBRYSettings)
            return
        Shared = LBRYSettings["preferences"]["shared"]["value"]
        ShowMature = Shared["settings"]["show_mature"]
        LBRYGTKSettings, RawTime = Shared["LBRY-GTK"], self.DateTimer.GetTime()
        Time, TimeText = str(RawTime), "equals "
        Texts = ["less than", "more than", "less or equals", "more or equals"]
        Times = ["<", ">", "<=", ">="]
        if Time != "-1":
            Inequality = self.Inequality.get_active()
            if Inequality != 2:
                if 2 < Inequality:
                    Inequality -= 1
                TimeText, Time = Texts[Inequality], Times[Inequality] + Time

        Data = {"text": self.Text.get_text(), "timestamp": Time}
        Data["channel"] = self.Channel.get_text()

        if not ShowMature:
            LBRYGTKSettings["NotTags"].append("mature")

        for Index in self.TaggersDict:
            Language = False
            try:
                Tags = self.TaggersDict[Index][1].Tags
            except:
                Tags = self.TaggersDict[Index][1].Tagger.Tags
                Language = True
            Data[Index] = copy.deepcopy(Tags)
            if Index == "not_tags":
                for Tag in Data[Index]:
                    if Tag in LBRYGTKSettings["NotTags"]:
                        Data[Index].remove(Tag)
            if Index == "not_channel_ids":
                for Tag in Data[Index]:
                    if Tag in LBRYGTKSettings["NotChannels"]:
                        Data[Index].remove(Tag)
            if Language:
                for SubIndex in range(len(Data[Index])):
                    Data[Index][SubIndex] = Data[Index][SubIndex].split("/")[0]

        Order = self.Orderer.Get()
        if Order:
            Data["order_by"] = [Order]
        else:
            Data["order_by"] = []

        ClaimType = self.ClaimType.get_active_text().lower()

        if ClaimType != "all":
            Data["claim_type"] = ClaimType

        Title = ""
        if Data["text"] != "":
            Title = "Text: " + Data["text"] + ", "
        if Data["timestamp"] != "-1":
            Time = str(datetime.datetime.fromtimestamp(RawTime))
            Title += "Time: " + TimeText + Time + ", "
        if "order_by" in Data:
            Title += self.Orderer.Print()
        if "claim_type" in Data:
            Title += "Type: %s, " % Data["claim_type"]

        for Index in self.TaggersDict:
            if Data[Index] != []:
                NewIndex = Index.replace("_", " ").title()
                NewData = copy.deepcopy(Data[Index])
                if Index in self.LanguagerList:
                    for ItemIndex in range(len(NewData)):
                        Item = NewData[ItemIndex]
                        for Language in Languages:
                            if Language[0] == Item:
                                NewData[ItemIndex] = (
                                    Language[0] + "/" + Language[1]
                                )
                                break
                Title += NewIndex + ": " + ", ".join(NewData) + ", "

        Title = "Advanced Search - " + Title[:-2]
        Args = ["Search", "Content", Title, Data]
        if Event.button == Gdk.BUTTON_PRIMARY:
            threading.Thread(None, self.Lister.ButtonThread, None, Args).start()
        elif Event.button == Gdk.BUTTON_MIDDLE:
            self.AddPage(".", "", ["Advanced Search", Args])

    def on_SelectNext_activate(self, Widget):
        if (
            self.Grid.get_realized()
            or self.ContentView.get_realized()
            or self.NoContentNotice.get_realized()
        ):
            self.Selector.Forward()

    def on_SelectPrevious_activate(self, Widget):
        if (
            self.Grid.get_realized()
            or self.ContentView.get_realized()
            or self.NoContentNotice.get_realized()
        ):
            self.Selector.Backward()

    def NoToggleBefore(self, Widget):
        Widget.set_name(str(Widget.get_active()))

    def NoToggleAfter(self, Widget):
        Widget.set_active(Widget.get_name() == "True")

    def on_Advanced_activate(self, Widget):
        Widget.get_children()[0].set_visible(not Widget.get_expanded())
