################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

from Source import Places

Orderings = ["creation_height", "name", "trending_mixed"]


class Order:
    def __init__(self):
        Builder = Gtk.Builder.new_from_file(Places.GladeDir + "Order.glade")
        Builder.connect_signals(self)
        self.Order = Builder.get_object("Order")
        self.UseOrdering = Builder.get_object("UseOrdering")
        self.OrderBy = Builder.get_object("OrderBy")
        self.Direction = Builder.get_object("Direction")
        self.UseOrderingActive = Builder.get_object("UseOrderingActive")
        self.OrderByActive = Builder.get_object("OrderByActive")
        self.DirectionActive = Builder.get_object("DirectionActive")

    def StopScroll(self, Widget, Event):
        Widget.emit_stop_by_name("scroll-event")

    def Get(self):
        if self.UseOrdering.get_active():
            Order = Orderings[self.OrderBy.get_active()]
            if self.Direction.get_active() == 1:
                Order = "^" + Order
            return Order
        return ""

    def Set(self, Ordering):
        if Ordering == "":
            self.UseOrdering.set_active(False)
            self.OrderBy.set_active(0)
            self.Direction.set_active(0)
            return
        self.UseOrdering.set_active(True)
        if Ordering.startswith("^"):
            self.Direction.set_active(1)
            Ordering = Ordering[1:]
        else:
            self.Direction.set_active(0)
        for Index in range(len(Orderings)):
            if Ordering == Orderings[Index]:
                self.OrderBy.set_active(Index)

    def Print(self):
        if not self.UseOrdering.get_active():
            return ""
        return (
            "Order by: "
            + self.OrderBy.get_active_text().lower()
            + ", "
            + self.Direction.get_active_text().lower()
            + ", "
        )

    def get_value(self):
        return self.Get()

    def set_value(self, Value):
        self.Set(Value)
