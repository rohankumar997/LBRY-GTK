################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import gi, threading, json, copy, pathlib, queue

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GLib, Gdk

from flbry import settings, connect, channel, meta

from Source import Places, Startup, SettingsUpdate, Popup, Move, SelectUtil
from Source.Channels import Channels
from Source.Tag import Tag
from Source.Order import Order
from Source.KeyBind import KeyBind
from Source.Select import Select
from Source.Language import Language


def Get():
    with open(Places.ConfigDir + "Session.json", "r") as File:
        Session = json.load(File)
    LBRYSettings = settings.get(server=Session["Server"])
    if isinstance(LBRYSettings, str):
        return LBRYSettings
    LBRYSettings["Session"] = Session
    return LBRYSettings


def Set(LBRYSettings):
    Session = LBRYSettings["Session"]
    del LBRYSettings["Session"]
    settings.set(LBRYSettings, server=Session["Server"])


class Settings:
    def __init__(self, *args):
        (
            self.Window,
            self.Title,
            self.Stater,
            self.Startuper,
            self.Main,
        ) = args
        Builder = Gtk.Builder.new_from_file(Places.GladeDir + "Settings.glade")
        Builder.connect_signals(self)
        self.Settings = Builder.get_object("Settings")
        self.MatureContent = Builder.get_object("MatureContent")
        self.LocalTagBox = Builder.get_object("LocalTagBox")
        self.SharedTagBox = Builder.get_object("SharedTagBox")
        self.NotTagBox = Builder.get_object("NotTagBox")
        self.NotChannelBox = Builder.get_object("NotChannelBox")
        self.OrderBox = Builder.get_object("OrderBox")
        self.CommentChannelBox = Builder.get_object("CommentChannelBox")
        self.PresetHome = Builder.get_object("PresetHome")
        self.KeyBindBox = Builder.get_object("KeyBindBox")
        self.SelectPrevious = Builder.get_object("SelectPrevious")
        self.SelectNext = Builder.get_object("SelectNext")
        self.OpenOnCurrent = Builder.get_object("OpenOnCurrent")
        self.OpenOnNew = Builder.get_object("OpenOnNew")
        self.Import = Builder.get_object("Import")
        self.Export = Builder.get_object("Export")
        self.Save = Builder.get_object("Save")
        self.MoveLeft = Builder.get_object("MoveLeft")
        self.MoveRight = Builder.get_object("MoveRight")
        self.MoveUp = Builder.get_object("MoveUp")
        self.MoveDown = Builder.get_object("MoveDown")
        self.TagSelect = Builder.get_object("TagSelect")
        self.LanguageBox = Builder.get_object("LanguageBox")

        self.SettingFields = {}
        for Setting in Startup.DefaultSettings:
            self.SettingFields[Setting] = Builder.get_object(Setting)
        GetObject = Builder.get_object
        self.LBRYFields = {"save_resolved_claims": GetObject("SaveResolved")}
        self.LBRYFields["save_files"] = GetObject("SaveFiles")
        self.LBRYFields["save_blobs"] = GetObject("SaveBlobs")
        self.LBRYFields["download_dir"] = GetObject("DownloadPath")
        self.LBRYFields["blob_download_timeout"] = GetObject("BlobTimeout")
        self.LBRYFields["download_timeout"] = GetObject("DownloadTimeout")
        self.LBRYFields["hub_timeout"] = GetObject("HubTimeout")
        self.LBRYFields["node_rpc_timeout"] = GetObject("NodeRPCTimeout")
        self.LBRYFields["peer_connect_timeout"] = GetObject("PeerTimeout")

        self.SessionFields = {}
        for Setting in Startup.DefaultSession:
            if Setting.startswith("KB"):
                Check, Title = Setting[2:], ""
                for Index in range(len(Check)):
                    if Index != 0:
                        if (
                            Check[Index].isupper()
                            and Index + 1 < len(Check)
                            and Check[Index + 1].islower()
                        ):
                            Title += " "
                    Title += Check[Index]
                Label = Gtk.Label.new(Title)
                Label.set_halign(Gtk.Align.START)
                self.KeyBindBox.pack_start(Label, True, True, 0)

                KeyBinder = KeyBind()
                self.KeyBindBox.pack_start(KeyBinder.KeyBind, True, True, 0)
                self.SessionFields[Setting] = KeyBinder
            else:
                self.SessionFields[Setting] = Builder.get_object(Setting)
        self.KeyBindBox.show_all()
        self.Taggers = [self.LocalTagBox, self.SharedTagBox, self.NotTagBox]
        self.Taggers.extend([self.NotChannelBox])

        for Index in range(len(self.Taggers)):
            TagPlace = self.Taggers[Index]
            Tager = Tag(True)
            TagPlace.pack_start(Tager.Tag, True, True, 0)
            self.Taggers[Index] = Tager

        self.Languager = Language()
        self.LanguageBox.pack_start(self.Languager.Language, True, True, 0)

        self.Orderer = Order()
        self.OrderBox.pack_start(self.Orderer.Order, True, True, 0)
        GetObject("DownloadPath").get_children()[1].connect(
            "scroll-event", self.StopScroll
        )
        self.SettingFields["Order"] = self.Orderer
        self.SettingFields["NotTags"] = self.Taggers[2]
        self.SettingFields["NotChannels"] = self.Taggers[3]
        self.SettingFields["Languages"] = self.Languager.Tagger
        self.Channelser = Channels(False, self.Window)
        self.CommentChannel = self.Channelser.Channels
        self.CommentChannelBox.add(self.CommentChannel.get_parent())

        Items, AllChildren = [[], [], [], [], []], []
        self.Notebook = self.Settings.get_children()[0]
        ChildrenNumbers = []
        for Index in range(self.Notebook.get_n_pages() - 1):
            Box = self.Notebook.get_nth_page(Index).get_child().get_child()
            SelectUtil.PrepareNew(
                Items, Box.get_children(), self.Taggers, [self.Languager]
            )
            ChildrenNumbers.append(len(Items[0]))
        for Index in range(len(Items[2])):
            Enter, Number = Items[2][Index], 0
            for ChildrenNumber in ChildrenNumbers:
                if Index < ChildrenNumber:
                    break
                Number += 1
            Items[2][Index] = self.EnterLambda(Enter, Number)
        Items[0].extend([self.Import, self.Export, self.Save])
        self.Selector = Select(*Items, True)

    def on_TagSelect_activate(self, Widget):
        if self.Settings.get_realized():
            CurrentWidget = self.Selector.Widgets[self.Selector.CurrentItem]
            Name = CurrentWidget.get_parent().get_parent().get_name()
            if Name.startswith("Tag"):
                Index = int(Name.split("-")[1])
                self.Taggers[Index].Select()
            elif Name == "LanguageBox":
                self.Languager.Tagger.Select()

    def on_MoveLeft_activate(self, Widget):
        if self.Settings.get_realized():
            CurrentWidget = self.Selector.Widgets[self.Selector.CurrentItem]
            Name = CurrentWidget.get_parent().get_parent().get_name()
            if Name.startswith("Tag"):
                Index = int(Name.split("-")[1])
                Move.FlowBoxLeftRight(self.Taggers[Index].FlowBox, -1)
            elif Name == "LanguageBox":
                Move.FlowBoxLeftRight(self.Languager.Tagger.FlowBox, -1)

    def on_MoveRight_activate(self, Widget):
        if self.Settings.get_realized():
            CurrentWidget = self.Selector.Widgets[self.Selector.CurrentItem]
            Name = CurrentWidget.get_parent().get_parent().get_name()
            if Name.startswith("Tag"):
                Index = int(Name.split("-")[1])
                Move.FlowBoxLeftRight(self.Taggers[Index].FlowBox)
            elif Name == "LanguageBox":
                Move.FlowBoxLeftRight(self.Languager.Tagger.FlowBox)

    def on_MoveDown_activate(self, Widget):
        if self.Settings.get_realized():
            CurrentWidget = self.Selector.Widgets[self.Selector.CurrentItem]
            Name = CurrentWidget.get_parent().get_parent().get_name()
            if Name.startswith("Tag"):
                Index = int(Name.split("-")[1])
                Move.FlowBoxUpDown(self.Taggers[Index].FlowBox)
            elif Name == "LanguageBox":
                Move.FlowBoxUpDown(self.Languager.Tagger.FlowBox)

    def on_MoveUp_activate(self, Widget):
        if self.Settings.get_realized():
            CurrentWidget = self.Selector.Widgets[self.Selector.CurrentItem]
            Name = CurrentWidget.get_parent().get_parent().get_name()
            if Name.startswith("Tag"):
                Index = int(Name.split("-")[1])
                Move.FlowBoxUpDown(self.Taggers[Index].FlowBox, -1)
            elif Name == "LanguageBox":
                Move.FlowBoxUpDown(self.Languager.Tagger.FlowBox, -1)

    def on_OpenOnCurrent_activate(self, Widget):
        if self.Settings.get_realized():
            self.Selector.Activate(0)

    def on_OpenOnNew_activate(self, Widget):
        if self.Settings.get_realized():
            self.Selector.Activate(1)

    def EnterLambda(self, Function, Index):
        return lambda: self.Enter(Function, Index)

    def Enter(self, Function, Index):
        if Index != self.Notebook.get_current_page():
            self.Notebook.set_current_page(Index)
        Function()

    def on_SelectPrevious_activate(self, Widget):
        if self.Settings.get_realized():
            self.Selector.Backward()

    def on_SelectNext_activate(self, Widget):
        if self.Settings.get_realized():
            self.Selector.Forward()

    def StopScroll(self, Widget, Event):
        Widget.emit_stop_by_name("scroll-event")

    def GetValue(self, Widget):
        try:
            return Widget.get_value()
        except:
            try:
                return Widget.get_active()
            except:
                try:
                    return Widget.get_text()
                except:
                    try:
                        return Widget.get_filename()
                    except:
                        pass

    def SetValue(self, Widget, Value):
        try:
            Widget.set_value(Value)
        except:
            try:
                Widget.set_active(Value)
            except:
                try:
                    Widget.set_text(Value)
                except:
                    try:
                        Widget.set_filename(Value)
                    except:
                        pass

    def ShowSettings(self, OverWrite=True):
        if OverWrite:
            with open(Places.ConfigDir + "Session.json", "r") as File:
                self.Session = json.load(File)
        self.Loaded = False
        if self.Startuper.Started:
            if OverWrite:
                self.LBRYSetting = settings.get(server=self.Session["Server"])
            Settings = self.LBRYSetting["settings"]
            Shared = self.LBRYSetting["preferences"]["shared"]["value"]
            Local = self.LBRYSetting["preferences"]["local"]["value"]
            LBRYGTK = Shared["LBRY-GTK"]
            self.MatureContent.set_active(Shared["settings"]["show_mature"])
            for Setting in self.LBRYFields:
                self.SetValue(self.LBRYFields[Setting], Settings[Setting])

            for Setting in self.SettingFields:
                self.SetValue(self.SettingFields[Setting], LBRYGTK[Setting])

            self.Taggers[0].Append(Local["tags"])
            self.Taggers[1].Append(Shared["tags"])

            ChannelList = channel.channel_list(server=self.Session["Server"])
            self.Channelser.Update(ChannelList, LBRYGTK)
            self.on_HomeType_changed(self.SettingFields["HomeType"])
            self.Loaded = True

        for Setting in self.SessionFields:
            self.SetValue(self.SessionFields[Setting], self.Session[Setting])

        self.Replace("Settings")
        self.Title.set_text("Settings")

        self.SettingFields["AuthToken"].set_visibility(False)

    def on_Edit_button_press_event(self, Widget, Event):
        self.SettingFields["AuthToken"].set_visibility(
            not self.SettingFields["AuthToken"].get_visibility()
        )

    States = [
        ["Following", "[]"],
        ["YourTags", "[]"],
        ["Discover", "[]"],
        ["Library", "[]"],
        ["Library Search", "[]"],
        ["Collections", "[]"],
        ["Followed", "[]"],
        ["Uploads", "[]"],
        ["Channels", "[]"],
        ["NewPublication", "[]"],
        ["Settings", "[]"],
        ["Status", "[]"],
        ["Help", "[]"],
        ["Inbox", "[]"],
    ]

    def on_HomeType_changed(self, Widget):
        Children = Widget.get_parent().get_children()
        if Widget.get_active() == 0:
            for Index in range(len(self.States)):
                State = self.States[Index]
                if (
                    self.SettingFields["HomeFunction"].get_text() == State[0]
                    and self.SettingFields["HomeData"].get_text() == State[1]
                ):
                    self.PresetHome.set_active(Index)
                    break
            self.on_PresetHome_changed(self.PresetHome)
        else:
            for Index in range(2, len(Children)):
                Children[Index].set_visible(3 < Index)

    def on_MenuItem_activate(self, Widget, State):
        self.SettingFields["HomeFunction"].set_text(State[0])
        self.SettingFields["HomeData"].set_text(State[1])

    def on_HomeFromPageHistory_button_press_event(self, Widget, Event):
        List = self.Stater.Before()
        Length = len(List)
        if Length != 0:
            Menu = Gtk.Menu.new()
            for Index in range(Length):
                Item = List[Index]
                MenuItem = Gtk.MenuItem.new_with_label(Item["Title"])
                MenuItem.connect(
                    "activate",
                    self.on_MenuItem_activate,
                    self.Stater.Export(
                        Item["Function"],
                        Item["Data"],
                        True,
                    ),
                )
                Menu.attach(MenuItem, 0, 1, Length - Index - 1, Length - Index)
            Menu.show_all()
            Menu.popup_at_widget(
                Widget, Gdk.Gravity.SOUTH_WEST, Gdk.Gravity.NORTH_WEST
            )

    def on_PresetHome_changed(self, Widget):
        self.on_MenuItem_activate("", self.States[Widget.get_active()])

    def on_Export_button_press_event(self, Widget, Event):
        Dialog = Gtk.FileChooserDialog(
            "Export settings - LBRY-GTK",
            self.Window,
            Gtk.FileChooserAction.SAVE,
            (
                Gtk.STOCK_CANCEL,
                Gtk.ResponseType.CANCEL,
                Gtk.STOCK_SAVE,
                Gtk.ResponseType.ACCEPT,
            ),
        )
        Dialog.set_do_overwrite_confirmation(True)
        Filter = Gtk.FileFilter.new()
        Filter.set_name("Json files")
        Filter.add_pattern("*.json")
        Dialog.add_filter(Filter)
        Dialog.set_current_name("LBRY-GTK-Settings.json")
        Dialog.set_current_folder(str(pathlib.Path.home()))
        Ran = Dialog.run()

        while (
            Ran == Gtk.ResponseType.ACCEPT
            and not Dialog.get_filename().endswith(".json")
        ):
            Dialog.set_current_name(Dialog.get_current_name() + ".json")
            Ran = Dialog.run()

        if Ran == Gtk.ResponseType.ACCEPT and Dialog.get_filename().endswith(
            ".json"
        ):
            Args = [Dialog.get_filename()]
            threading.Thread(None, self.ExportThread, None, Args).start()
        Dialog.destroy()
        return True

    def ExportThread(self, Path):
        with open(Places.ConfigDir + "Session.json", "r") as File:
            self.Session = json.load(File)
        self.LBRYSetting = settings.get(server=self.Session["Server"])
        Table = copy.deepcopy(self.LBRYSetting)
        Table["Session"] = self.Session
        Json = json.dumps(Table, indent=4)
        with open(Path, "w") as File:
            File.write(Json)
        GLib.idle_add(self.ExportUpdate, True)

    def ExportUpdate(self, Success):
        Message = "The export was not successful."
        if Success:
            Message = "The export was successful."
        Popup.Message(Message, self.Window)

    def on_Import_button_press_event(self, Widget, Event):
        Dialog = Gtk.FileChooserDialog(
            "Import settings - LBRY-GTK",
            self.Window,
            Gtk.FileChooserAction.OPEN,
            (
                Gtk.STOCK_CANCEL,
                Gtk.ResponseType.CANCEL,
                Gtk.STOCK_OPEN,
                Gtk.ResponseType.ACCEPT,
            ),
        )
        Filter = Gtk.FileFilter.new()
        Filter.set_name("Json files")
        Filter.add_pattern("*.json")
        Dialog.add_filter(Filter)
        Dialog.set_current_folder(str(pathlib.Path.home()))
        if Dialog.run() == Gtk.ResponseType.ACCEPT:
            Args = [Dialog.get_filename()]
            threading.Thread(None, self.ImportThread, None, Args).start()
        Dialog.destroy()
        return True

    def ImportThread(self, Path):
        with open(Path, "r") as File:
            Table = json.load(File)
        self.LBRYSetting = copy.deepcopy(Table)
        del self.LBRYSetting["Session"]
        self.Session = copy.deepcopy(Table["Session"])
        GLib.idle_add(self.ImportUpdate, True)

    def ImportUpdate(self, Success):
        Message = "The export was not successful."
        if Success:
            Message = (
                "The export was successful. Click Save to save the settings."
            )
            self.ShowSettings(False)
        Popup.Message(Message, self.Window)

    def on_Save_button_press_event(self, Widget, Event):
        with open(Places.ConfigDir + "Session.json", "r") as File:
            self.Session = json.load(File)
        if self.Loaded:
            self.LBRYSetting = settings.get(server=self.Session["Server"])
            Settings = self.LBRYSetting["settings"]
            Shared = self.LBRYSetting["preferences"]["shared"]["value"]
            Local = self.LBRYSetting["preferences"]["local"]["value"]
            LBRYGTK = Shared["LBRY-GTK"]
            Shared["settings"]["show_mature"] = self.MatureContent.get_active()
            for Setting in self.LBRYFields:
                Settings[Setting] = self.GetValue(self.LBRYFields[Setting])

            for Setting in self.SettingFields:
                LBRYGTK[Setting] = self.GetValue(self.SettingFields[Setting])

            Local["tags"] = self.Taggers[0].Tags
            Shared["tags"] = self.Taggers[1].Tags

            try:
                LBRYGTK["CommentChannel"] = self.Channelser.Get()[0]
            except:
                pass

        for Setting in self.SessionFields:
            if Setting != "Server" and Setting != "Binary":
                self.Session[Setting] = self.GetValue(
                    self.SessionFields[Setting]
                )

        self.Session["NewServer"] = self.GetValue(self.SessionFields["Server"])
        self.Session["NewBinary"] = self.GetValue(self.SessionFields["Binary"])

        KeyBind = {}
        self.Duplicates = {}
        self.SameBindings = False
        for Setting in self.SessionFields:
            if Setting.startswith("KB") and self.Session[Setting] != "":
                if self.Session[Setting] in KeyBind:
                    self.SameBindings = True
                    if not KeyBind[self.Session[Setting]] in self.Duplicates:
                        self.Duplicates[KeyBind[self.Session[Setting]]] = []
                    self.Duplicates[KeyBind[self.Session[Setting]]].append(
                        Setting
                    )
                else:
                    KeyBind[self.Session[Setting]] = Setting

        GLib.idle_add(self.SaveHelper)

    def PrintDuplicates(self):
        Text = ""
        for Item in self.Duplicates:
            Text += Item[2:] + ": "
            for SubItem in self.Duplicates[Item]:
                Text += SubItem[2:] + ", "
            Text = Text[0:-2] + "\n"
        return Text[0:-1]

    def SaveHelper(self):
        Response = Gtk.ResponseType.OK
        if (
            self.Session["Server"] != self.Session["NewServer"]
            or self.Session["Binary"] != self.Session["NewBinary"]
        ):
            Dialog = Gtk.MessageDialog(
                self.Window, buttons=Gtk.ButtonsType.OK_CANCEL
            )
            Dialog.props.text = "Warning: changing Binary or Server only activates after a restart."
            Response = Dialog.run()
            Dialog.destroy()
        if self.SameBindings:
            Popup.Message(
                "There are duplicate key bindings. Change them before the settings can be saved:\n"
                + self.PrintDuplicates(),
                self.Window,
            )
        elif Response == Gtk.ResponseType.OK:
            threading.Thread(None, self.SaveThread).start()

    def SaveThread(self):
        if self.Loaded:
            JsonData = settings.set(
                self.LBRYSetting, server=self.Session["Server"]
            )
            if isinstance(JsonData, str):
                Popup.Error(JsonData, self.Window)
        with open(Places.ConfigDir + "Session.json", "w") as File:
            json.dump(self.Session, File)
        GLib.idle_add(SettingsUpdate.UpdateWidth, self.Window, self.Session)
        GLib.idle_add(
            SettingsUpdate.ChangeMenu,
            self.Window,
            self.Session["MenuType"],
            self.Session["MenuIcon"],
        )
        GLib.idle_add(
            SettingsUpdate.ChangeMeta,
            self.Window,
            self.Session["EnableMetaService"],
        )
        GLib.idle_add(
            SettingsUpdate.WindowAcceleratorCreate,
            self.Window,
            self.Main,
        )

    def NoToggleBefore(self, Widget):
        Widget.set_name(str(Widget.get_active()))

    def NoToggleAfter(self, Widget):
        Widget.set_active(Widget.get_name() == "True")
