################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import gi, threading

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Pango, Gdk

from Source import Timestamp, Places
from Source.TextColumn import TextColumn
from Source.Icons import LBCLabel


class Box:
    def __init__(self, *args):
        (
            self.Pixbuf,
            self.Row,
            self.Publicationer,
            TitleRows,
            AuthorRows,
            Padding,
            HiddenExtra,
            self.AddPage,
            self.Stater,
        ) = args
        Builder = Gtk.Builder.new_from_file(Places.GladeDir + "Box.glade")
        Builder.connect_signals(self)

        self.Box = Builder.get_object("Box")
        self.Thumbnail = Builder.get_object("Thumbnail")

        if isinstance(self.Row[3], type(None)):
            self.Row[3] = ""
        if isinstance(self.Row[4], type(None)):
            self.Row[4] = ""

        ToolTip = self.Row[3]
        if ToolTip != "" and self.Row[4] != "":
            ToolTip += " - "
        ToolTip += self.Row[4]

        self.Box.set_tooltip_text(ToolTip)

        Grid = self.Box.get_child()
        Grid.set_margin_start(Padding)
        Grid.set_margin_end(Padding)
        Grid.set_margin_top(Padding)
        Grid.set_margin_bottom(Padding)

        Width = self.Pixbuf.get_width()

        Title = TextColumn(self.Row[4], Width, TitleRows, True, True)
        Builder.get_object("TitleBox").add(Title.Column)

        Author = TextColumn(self.Row[3], Width, AuthorRows, True, True)
        Builder.get_object("AuthorBox").add(Author.Column)

        self.Confirmations = Builder.get_object("Confirmations")
        self.ConfirmationsLabel = Builder.get_object("ConfirmationsLabel")
        self.Amount = Builder.get_object("Amount")
        self.AmountLabel = Builder.get_object("AmountLabel")
        self.IsTip = Builder.get_object("IsTip")
        self.LBC = Builder.get_object("LBC")

        if HiddenExtra:
            ToHide = [self.Confirmations, self.ConfirmationsLabel, self.Amount]
            ToHide.extend([self.AmountLabel, self.IsTip, self.LBC])
            for Widget in ToHide:
                Widget.set_no_show_all(True)
        else:
            self.Confirmations.set_label(str(self.Row[0]))
            self.Amount.set_label(str(self.Row[1]))
            self.IsTip.set_active(self.Row[2])
            self.LBC.set_from_pixbuf(LBCLabel)
        Time = Timestamp.Ago(self.Row[5])
        Tooltip = Timestamp.GetDate(self.Row[5])
        Builder.get_object("Time").set_label(Time)
        Builder.get_object("Time").set_tooltip_text(Tooltip)
        Builder.get_object("Type").set_label(self.Row[6])

    def on_Box_button_press_event(self, Widget, Event):
        if Event.button != 8 and Event.button != 9:
            Url = [self.Row[-3]]
            if Url != [""]:
                if Event.button == Gdk.BUTTON_PRIMARY:
                    threading.Thread(
                        None, self.Publicationer.GetPublication, None, Url
                    ).start()
                elif Event.button == Gdk.BUTTON_MIDDLE:
                    self.AddPage(
                        ".",
                        "",
                        self.Stater.Export(
                            self.Publicationer.GetPublication, Url
                        ),
                    )

    def on_Box_enter_notify_event(self, Widget, Discard=""):
        FlowChild = Widget.get_parent()
        FlowChild.get_parent().select_child(FlowChild)

    def on_Box_leave_notify_event(self, Widget, Discard=""):
        Widget.get_parent().get_parent().unselect_all()

    def NoToggleBefore(self, Widget):
        Widget.set_name(str(Widget.get_active()))

    def NoToggleAfter(self, Widget):
        Widget.set_active(Widget.get_name() == "True")
