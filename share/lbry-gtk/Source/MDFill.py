################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import re, gi, threading, json

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GdkPixbuf, Gdk, Pango

from flbry import url

from Source import Image, Places, Settings, MDParse, Popup
from Source.Open import Open

Items = ["<a", "<hr", "&lt;!--", "<img", "<blockquote>"]

Entities = MDParse.Entities


def GetIndex(TextBuffer, OldIndex):
    Index, StartIter = OldIndex, TextBuffer.get_start_iter()
    GetIterAtOffset = TextBuffer.get_iter_at_offset
    Text = TextBuffer.get_text(StartIter, GetIterAtOffset(Index), True)
    for Entity in Entities:
        EntityCount = 0
        Test = Text.find(Entities[Entity])
        while Test != -1:
            Index -= len(Entity) - 1
            EntityCount += 1
            Text = TextBuffer.get_text(StartIter, GetIterAtOffset(Index), True)
            Text = Text.replace(Entities[Entity], "", EntityCount)
            Test = Text.find(Entities[Entity])
    return Index


def Empty(*Args):
    return ""


def RemoveA(Start, Text):
    LinkText = Text[Start:]
    HrefStart = LinkText.find('"') + 1
    Link = LinkText[HrefStart : LinkText.find('"', HrefStart)]
    LabelStart = LinkText.find(">") + 1
    LabelEnd = LinkText.find("</a>", LabelStart)
    End = LabelEnd + 4
    Label = LinkText[LabelStart:LabelEnd]
    CleanLabel = Label
    for Entity in Entities:
        CleanLabel = CleanLabel.replace(Entity, Entities[Entity])
    Length, Insert = len(re.sub("<[^>]*>", "", CleanLabel)), ""
    if Find(Label):
        Length = 2
    elif Length == 0:
        Insert = Link
        Length = len(Link)
    return (Text[:Start] + Insert + Label + LinkText[End:], [Link, Length])


def RemoveImg(Start, Text):
    LinkText = Text[Start:]
    HrefStart = LinkText.find('"') + 1
    Link = LinkText[HrefStart : LinkText.find('"', HrefStart)]
    End = LinkText.find("/>") + 2
    return (Text[:Start] + " " + LinkText[End:], Link)


def RemoveHR(Start, Text):
    LinkText = Text[Start:]
    End = LinkText.find("/>") + 2
    return (Text[:Start] + LinkText[End:], "")


def RemoveC(Start, Text):
    return (Text[:Start] + Text[(Text.find("--&gt;") + 6) :], "")


def RemoveQuotes(Start, Text):
    QuoteText = Text[Start:]
    QuoteStart = QuoteText.find(">") + 1
    QuoteTextCopy = QuoteText
    Starts, Ends = 0, 0
    while True:
        PossibleEnd = QuoteTextCopy.find("</blockquote>")
        PossibleStart = QuoteTextCopy.find("<blockquote>")
        if PossibleStart != -1 and PossibleStart < PossibleEnd:
            Starts += 1
            QuoteTextCopy = QuoteTextCopy.replace("<blockquote>", "", 1)
        elif PossibleEnd != -1:
            Ends += 1
            QuoteTextCopy = QuoteTextCopy.replace("</blockquote>", "", 1)
        if Starts == Ends:
            break
    End = PossibleEnd + Starts * 12 + Ends * 13
    Quote = QuoteText[QuoteStart : End - 13]
    CleanQuote = re.sub("<[^>]*>", "", Quote)
    return (Text[:Start] + Quote + QuoteText[End:], len(CleanQuote))


Removers = {"<a": RemoveA, "<hr": RemoveHR, "&lt;!--": RemoveC}
Removers["<img"], Removers["<blockquote>"] = RemoveImg, RemoveQuotes


def LinkClick(
    TextTag,
    Widget,
    Event,
    Iter,
    GetPublication,
    LinkCommand,
    Path,
    NoResolve,
    AddPage,
    Markdown,
    Button="",
):
    Link = TextTag.get_property("name").split(" ")[0]
    if Event.type == Gdk.EventType.BUTTON_PRESS:
        Widget.set_name(Link)
    if Event.type == Gdk.EventType.BUTTON_RELEASE:
        if Button == "":
            Button = Event.button.button
        Name = Widget.get_name().split(" ")[0]
        if Name == Link:
            if Name.startswith(".") and Name.endswith(".md"):
                try:
                    with open(Path + Name[2:], "r") as File:
                        Text = File.read()
                    Window = Widget.get_window(Gtk.TextWindowType.TEXT)
                    Markdown.Text = Text
                    Markdown.Fill()
                except:
                    pass
            else:
                with open(Places.ConfigDir + "Session.json", "r") as File:
                    Session = json.load(File)
                if NoResolve or isinstance(
                    url.get([Name], server=Session["Server"])[0], str
                ):
                    Open(Name, LinkCommand)
                else:
                    if Button == Gdk.BUTTON_PRIMARY:
                        GetPublication(Name)
                    elif Button == Gdk.BUTTON_MIDDLE:
                        AddPage(".", "", ["Publication", [Name]])
            Widget.set_name("")


def InsertA(
    TextView, Index, Data, Settings, Rotate, IfComment, GetPublication, *args
):
    Link, Length = Data

    if Length != 0:
        TextBuffer = TextView.get_buffer()
        Index = GetIndex(TextBuffer, Index)
        for Entity in Entities:
            Link = Link.replace(Entity, Entities[Entity])
        StartIter = TextBuffer.get_iter_at_offset(Index)
        TagTable = TextBuffer.get_tag_table()
        LinkTag = TagTable.lookup(Link + " " + str(Rotate))
        if LinkTag == None:
            LinkTag = TextBuffer.create_tag(Link + " " + str(Rotate))
        if Rotate == 0:
            LinkTag.set_property("underline", Pango.Underline.DOUBLE)
        EndIter = TextBuffer.get_iter_at_offset(Index + Length)
        TextBuffer.apply_tag(LinkTag, StartIter, EndIter)
        TextBuffer.apply_tag_by_name("Link", StartIter, EndIter)
        LinkTag.connect(
            "event", LinkClick, GetPublication, Settings["LinkCommand"], *args
        )
        return LinkTag


def InsertImg(
    TextView,
    Index,
    Link,
    Settings,
    Rotate,
    IfComment,
    GetPublication,
    Path,
    *args
):
    GetResolution, FillPixbuf = Image.GetUrlResolution, Image.FillPixbuf
    if Link.startswith("."):
        GetResolution, Link = Image.GetPathResolution, Path + Link[2:]
        FillPixbuf = Image.FillPixbufFill
    if IfComment:
        ImageType = Settings["CommentImageType"]
        MaxImageWidth = Settings["CommentImageWidth"]
        MaxImageHeight = Settings["CommentImageHeight"]
        ImageScale = Settings["CommentImageScale"]
    else:
        ImageType = Settings["ImageType"]
        MaxImageWidth = Settings["ImageWidth"]
        MaxImageHeight = Settings["ImageHeight"]
        ImageScale = Settings["ImageScale"]

    TextBuffer = TextView.get_buffer()
    Index = GetIndex(TextBuffer, Index)
    Iter = TextBuffer.get_iter_at_offset(Index)
    TextBuffer.insert(Iter, " ")
    Args = [Link, InsertImgUpdate, TextView, Index, Link, ImageType, ImageScale]
    Args.extend([MaxImageWidth, MaxImageHeight, FillPixbuf])
    threading.Thread(None, GetResolution, None, Args, daemon=True).start()


def InsertImgUpdate(
    TextView,
    Index,
    Link,
    ImageType,
    ImageScale,
    MaxImageWidth,
    MaxImageHeight,
    FillPixbuf,
    Size,
):
    if ImageType == 0:
        Pixbuf = GdkPixbuf.Pixbuf.new(GdkPixbuf.Colorspace.RGB, True, 8, *Size)
    elif ImageType == 2:
        Resolution = [*Size]
        Resolution[0] *= ImageScale
        Resolution[1] *= ImageScale
        Pixbuf = GdkPixbuf.Pixbuf.new(
            GdkPixbuf.Colorspace.RGB, True, 8, *Resolution
        )
    else:
        ImageWidth, ImageHeight = Size
        WidthRatio = MaxImageWidth / ImageWidth
        HeightRatio = MaxImageHeight / ImageHeight

        Ratio = min(WidthRatio, HeightRatio)

        NewWidth, NewHeight = int(ImageWidth * Ratio), int(ImageHeight * Ratio)
        if ImageWidth < NewWidth:
            NewWidth, NewHeight = ImageWidth, ImageHeight
        Pixbuf = GdkPixbuf.Pixbuf.new(
            GdkPixbuf.Colorspace.RGB, True, 8, NewWidth, NewHeight
        )
    GImage = Gtk.Image.new_from_pixbuf(Pixbuf)
    FillPixbuf(Link, Pixbuf, GImage.set_from_pixbuf, Pixbuf)
    TextBuffer = TextView.get_buffer()
    StartIter = TextBuffer.get_iter_at_offset(Index)
    EndIter = TextBuffer.get_iter_at_offset(Index + 1)
    TextBuffer.delete(StartIter, EndIter)
    Anchor = TextBuffer.create_child_anchor(StartIter)
    TextView.add_child_at_anchor(GImage, Anchor)
    TextView.show_all()


def HRDraw(TextView, Discard, Widget, Index):
    TextBuffer = TextView.get_buffer()
    Iter = TextBuffer.get_iter_at_offset(Index)
    Location = TextView.get_iter_location(Iter)
    Widget.set_margin_top(Location.y + Location.height)
    Widget.set_size_request(TextView.get_allocated_width(), -1)


def InsertHR(TextView, Index, *args):
    Separator = Gtk.Separator.new(Gtk.Orientation.HORIZONTAL)
    Separator.set_valign(Gtk.Align.START)
    TextView.get_parent().get_parent().add_overlay(Separator)
    TextView.connect("draw", HRDraw, Separator, Index)


def QuoteDraw(TextView, Discard, Widget, StartIndex, EndIndex):
    TextBuffer = TextView.get_buffer()
    StartIter = TextBuffer.get_iter_at_offset(StartIndex + 1)
    EndIter = TextBuffer.get_iter_at_offset(EndIndex)
    MaxHeight = TextView.get_allocated_height()
    EndLocation = TextView.get_iter_location(EndIter)
    Widget.set_margin_top(TextView.get_iter_location(StartIter).y)
    if 0 < MaxHeight - EndLocation.height - EndLocation.y:
        Widget.set_margin_bottom(MaxHeight - EndLocation.height - EndLocation.y)
    Widget.show()


def InsertQuote(TextView, Index, Length, *args):
    TextBuffer = TextView.get_buffer()
    TagTable = TextBuffer.get_tag_table()
    Separator = Gtk.Separator.new(Gtk.Orientation.VERTICAL)
    Separator.set_halign(Gtk.Align.START)
    Separator.set_no_show_all(True)
    Separator.hide()
    Index = GetIndex(TextBuffer, Index)
    TextView.connect("draw", QuoteDraw, Separator, Index, Index + Length)
    TextView.get_parent().get_parent().add_overlay(Separator)
    StartIter = TextBuffer.get_iter_at_offset(Index)
    EndIter = TextBuffer.get_iter_at_offset(Index + Length)
    for Level in range(10, 0, -1):
        if StartIter.has_tag(TagTable.lookup("Quote" + str(Level))):
            if Level == 10:
                Separator.set_margin_start(270)
                TextBuffer.apply_tag_by_name("Quote10", StartIter, EndIter)
            else:
                Separator.set_margin_start(Level * 30)
                TextBuffer.apply_tag_by_name(
                    "Quote" + str(Level + 1), StartIter, EndIter
                )
            break
    if not StartIter.has_tag(TagTable.lookup("Quote1")):
        TextBuffer.apply_tag_by_name("Quote1", StartIter, EndIter)


Inserters = {"<a": InsertA, "<hr": InsertHR, "&lt;!--": Empty}
Inserters["<img"], Inserters["<blockquote>"] = InsertImg, InsertQuote


def Find(Text):
    Length, ClosestItem = len(Text), ""
    for Item in Items:
        if Text.find(Item) != -1 and Text.find(Item) < Length:
            Length, ClosestItem = Text.find(Item), Item
    if Length != len(Text):
        return ClosestItem
    return False


def Count(Replacings, Type):
    Number = 0
    for Replacing in Replacings:
        if Replacing[0] == Type:
            Number += 1
    return Number


def Fill(Text, TextView, Window, Links, *args):
    LBRYSettings = Settings.Get()
    if isinstance(LBRYSettings, str):
        Popup.Error(LBRYSettings, Window)
        return
    LBRYSettings = LBRYSettings["preferences"]["shared"]["value"]["LBRY-GTK"]
    Text, Replacings = MDParse.Parse(Text), []
    while Find(Text):
        Item = Find(Text)
        Start = Text.find(Item)
        Text, Data = Removers[Item](Start, Text)
        Length = len(
            re.sub("</", "", re.sub("<[^>]*>", "", Text[:Start]))
        ) + Count(Replacings, "<img")
        Replacings.append([Item, Length, Data])

    TextBuffer = TextView.get_buffer()
    TextBuffer.set_text("")
    Text += "\n"
    TextBuffer.insert_markup(TextBuffer.get_start_iter(), Text, -1)
    Rotate = 0
    for Replacing in Replacings:
        Length, Data = Replacing[1], Replacing[2]
        Link = Inserters[Replacing[0]](
            TextView, Length, Data, LBRYSettings, Rotate, *args
        )
        if Link:
            Rotate += 1
            Links.append(Link)
