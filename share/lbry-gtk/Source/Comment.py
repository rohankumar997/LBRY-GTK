################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

from flbry import error

from Source import Image, Places, Timestamp, Popup, SelectUtil
from Source.Open import Open
from Source.Channels import Channels
from Source.Markdown import Markdown
from Source.CommentControlTop import CommentControlTop
from Source.CommentControlMiddle import CommentControlMiddle
from Source.CommentControlBottom import CommentControlBottom
from Source.Thumbnail import Thumbnail
from Source.Select import Select

Notifies = ["style", "events", "valign", "halign", "hexpand", "name", "vexpand"]
Notifies.extend(["width-request", "resize-mode", "spacing", "margin", "window"])
Notifies.extend(["visible", "opacity", "is-focus", "sensitive", "homogeneous"])
Messages = [[0, 0], [1, 0], [0, 1], [0, 2], [0, 3], [0, 4], [0, 5], [0, 6]]
Messages.extend([[0, 7], [1, 8], [0, 9], [0, 10], [0, 11], [0, 12], [0, 13]])
Promo = "\n\nSent from [*LBRY-GTK*](https://codeberg.org/MorsMortium/LBRY-GTK)"


class Comment:
    def __init__(self, *args):
        (
            self.ShowHiderTexts,
            self.ClaimID,
            self.Box,
            self.Row,
            self.Reactions,
            self.CommentServer,
            HiddenUI,
            Start,
            self.Settings,
            self.ChannelList,
            PChannel,
            RowQueue,
            self.Window,
            self.SingleComment,
            self.GetPublication,
            self.Stater,
            self.AddPage,
            self.CommentExpander,
        ) = args

        Builder = Gtk.Builder.new_from_file(Places.GladeDir + "Comment.glade")
        Builder.connect_signals(self)
        self.Comment = Builder.get_object("Comment")
        self.CommentTextBox = Builder.get_object("CommentTextBox")
        self.HideDislikedBox = Builder.get_object("HideDislikedBox")
        self.RepliesFrame = Builder.get_object("RepliesFrame")
        self.ReplySeparator = Builder.get_object("ReplySeparator")
        self.RepliesExpander = Builder.get_object("RepliesExpander")
        self.RepliesBox = Builder.get_object("RepliesBox")
        self.ProfileBox = Builder.get_object("ProfileBox")
        self.PreviewBox = Builder.get_object("PreviewBox")
        self.CCTopBox = Builder.get_object("CommentControlTopBox")
        self.CCMiddleBox = Builder.get_object("CommentControlMiddleBox")
        self.CCBottomBox = Builder.get_object("CommentControlBottomBox")
        self.ReplyScrolled = Builder.get_object("ReplyScrolled")
        self.ReplyText = Builder.get_object("ReplyText")
        self.UnHideButton = Builder.get_object("UnHideButton")
        self.ProfileActive = Builder.get_object("ProfileActive")
        self.CommentActive = Builder.get_object("CommentActive")
        self.PreviewActive = Builder.get_object("PreviewActive")

        self.Channelser = Channels(
            False,
            self.Window,
            self.ChannelList,
            self.Settings,
        )
        self.Channels = self.Channelser.Channels

        self.Markdowns = [{"Text": self.Row[3], "Box": self.CommentTextBox}]
        self.Markdowns.append({"Text": "", "Box": self.PreviewBox})

        for MarkdownItem in self.Markdowns:
            self.MarkdownItems(MarkdownItem)

        # The comment is being checked here whether it should
        # be hidden or not
        LikeBoost = max(5, self.Reactions[0] * 5)
        Hide = LikeBoost <= self.Reactions[1] and self.Settings["HideDisliked"]
        self.CommentTextBox.set_visible(not Hide)
        self.HideDislikedBox.set_visible(Hide)

        self.CCBottomer = CommentControlBottom(
            self.Window,
            self.Row,
            self.Channelser,
            self.Markdowns,
            self.Box,
            self.ClaimID,
            self.CommentServer,
            self.ChannelList,
            self.SingleComment,
            self.Settings,
            self.RepliesFrame,
            self.RepliesBox,
            self.RepliesExpander,
            HiddenUI,
            PChannel,
            self.ReplyText,
            Promo,
            self.on_ReplyText_key_release_event,
            self.CommentExpander,
        )
        self.CCBottomBox.pack_start(
            self.CCBottomer.CommentControlBottom, True, True, 0
        )
        self.ChannelsBox2 = self.CCBottomer.ChannelsBox2
        self.Post, self.Save = self.CCBottomer.Post, self.CCBottomer.Save
        self.Tip = self.CCBottomer.Tip
        self.TipLabel1 = self.CCBottomer.TipLabel1
        self.TipLabel2 = self.CCBottomer.TipLabel2

        self.CCToper = CommentControlTop(
            self.Window,
            self.Row,
            self.GetPublication,
            self.ChannelList,
            self.CommentServer,
            self.Box,
            self.ShowHiderTexts,
            self.Markdowns,
            self.Comment,
            self.ReplyText,
            self.ReplyScrolled,
            self.PreviewBox,
            self.Save,
            self.Post,
            self.on_ReplyText_key_release_event,
            self.Stater,
            self.AddPage,
        )
        self.CCTopBox.pack_start(self.CCToper.CommentControlTop, True, True, 0)
        self.Channel, self.Support = self.CCToper.Channel, self.CCToper.Support
        self.CommentDate = self.CCToper.CommentDate
        self.Edit, self.Delete = self.CCToper.Edit, self.CCToper.Delete
        self.LBC1, self.LBC2 = self.CCToper.LBC1, self.CCToper.LBC2
        self.Value, self.Content = self.CCToper.Value, self.CCToper.Content
        self.ContentIcon = self.CCToper.ContentIcon
        self.Value.set_label(str(self.Row[8]))
        self.Content.set_label(str(self.Row[9]))

        self.CCMiddler = CommentControlMiddle(
            self.Window,
            self.Row,
            self.Reactions,
            self.Channelser,
            self.Edit,
            self.CommentServer,
            self.ReplyScrolled,
            self.Post,
            self.PreviewBox,
            self.Tip,
            self.TipLabel1,
            self.TipLabel2,
            PChannel,
            self.ChannelList,
            self.CCToper.EditImage,
        )
        self.CCMiddleBox.pack_start(
            self.CCMiddler.CommentControlMiddle, True, True, 0
        )

        self.Dislike, self.Like = self.CCMiddler.Dislike, self.CCMiddler.Like
        self.LikeNumber = self.CCMiddler.LikeNumber
        self.DislikeNumber = self.CCMiddler.DislikeNumber
        self.Reply, self.Heart = self.CCMiddler.Reply, self.CCMiddler.Heart
        self.ChannelsBox1 = self.CCMiddler.ChannelsBox1

        self.CCBottomer.on_Edit_button_press_event = (
            self.CCToper.on_Edit_button_press_event
        )
        self.CCBottomer.Edit = self.Edit
        self.CCBottomer.on_Reply_button_press_event = (
            self.CCMiddler.on_Reply_button_press_event
        )
        self.CCBottomer.Reply = self.Reply

        Widgets = [self.Channel, self.Edit, self.Delete, self.UnHideButton]
        MarkdownIndexes = [4, -1]

        if self.Settings["Profile"]:
            self.Thumbnailer = Thumbnail(
                self.Window,
                self.Settings["ProfileWidth"],
                self.Settings["ProfileHeight"],
            )
            self.ProfileBox.pack_start(
                self.Thumbnailer.Thumbnail, True, True, 0
            )
            self.Thumbnailer.Url = self.Row[6]
            self.ProfileBox.set_no_show_all(False)
            self.ProfileActive.set_no_show_all(False)
            Widgets.extend([self.ProfileBox, self.ProfileActive])
            Widgets.append(self.Thumbnailer.ThumbnailOpen)
            MarkdownIndexes[0] += 3

        for Reaction in [self.Heart, self.Like, self.Dislike]:
            Widgets.append(Reaction.get_children()[0])
        Widgets.extend([self.Reply, self.Channels, self.Channelser.Active])
        Widgets.extend([self.ReplyText, self.Tip, self.Post, self.Save])

        for Index in range(len(self.Markdowns)):
            Markdowner = self.Markdowns[Index]["Markdowner"]
            GrandParent = Markdowner.Document.get_parent().get_parent()
            for Child in GrandParent.get_children():
                if Child.get_name() == "Active":
                    Widgets.insert(MarkdownIndexes[Index], Child)
                    break
            Widgets.insert(MarkdownIndexes[Index], Markdowner.Activate)
            Widgets.insert(MarkdownIndexes[Index], Markdowner.Document)

        self.Selector = Select(*SelectUtil.PrepareWidgets(Widgets))

        self.RepliesFrame.set_margin_start(int(self.Settings["ReplyIndent"]))

        ReplySeparatorWidth = int(self.Settings["ReplySeparator"])
        self.ReplySeparator.set_size_request(ReplySeparatorWidth, 0)

        for Channel in self.ChannelList:
            if Channel[3] == PChannel:
                self.Heart.set_no_show_all(False)
                self.Heart.set_sensitive(True)
                break

        if self.Reactions[4]:
            self.Heart.set_no_show_all(False)

        if HiddenUI:
            self.ChannelsBox2.add(self.Channels.get_parent())
            ToShow = [self.ReplyScrolled, self.Post, self.Tip, self.TipLabel1]
            ToShow.extend([self.TipLabel2, self.PreviewBox.get_parent()])
            for Widget in ToShow:
                Widget.set_visible(True)
            ToHide = [self.Channel, self.Support, self.CommentDate, self.Delete]
            ToHide.extend([self.Like, self.Heart, self.Edit, self.ChannelsBox1])
            ToHide.extend([self.CommentTextBox, self.ProfileActive, self.Reply])
            ToHide.extend([self.Dislike, self.ProfileBox, self.HideDislikedBox])
            ToHide.extend([self.Save, self.LikeNumber, self.LBC1, self.Content])
            ToHide.extend([self.CommentActive, self.Value, self.DislikeNumber])
            ToHide.extend([self.ContentIcon, self.LBC2])
            for Widget in ToHide:
                Widget.set_no_show_all(True)
                Widget.hide()
        else:
            self.ChannelsBox2.set_no_show_all(True)
            self.ChannelsBox2.hide()
            self.ChannelsBox1.add(self.Channels.get_parent())

        if Start:
            self.Box.reorder_child(self.Comment, 0)

        self.Channel.set_label(self.Row[2])

        for Widget in [self.Support, self.LBC1]:
            Widget.set_visible(self.Row[1] != 0)

        self.Support.set_label(str(self.Row[1]))

        self.CommentDate.set_label(Timestamp.Ago(self.Row[7]))
        self.CommentDate.set_tooltip_text(Timestamp.GetDate(self.Row[7]))

        self.ShowHiderTexts.append(self.ReplyText)
        if self.Row[2] != "":
            for Channel in self.ChannelList:
                if Channel[3] == self.Row[5]:
                    self.Edit.set_no_show_all(False)
                    self.Delete.set_no_show_all(False)
                    break
        self.Box.add(self.Comment)
        self.MarkdownCommands = [self.MarkdownDown, self.MarkdownUp]
        self.Box.show_all()
        if self.Row[0] != 0:
            self.RepliesFrame.set_no_show_all(False)
            self.RepliesExpander.set_label("Replies (%s)" % str(self.Row[0]))
            self.RepliesFrame.show_all()
            RowQueue.put(self.RepliesBox)

    def GetMarkdown(self):
        CurrentWidget = self.Selector.Widgets[self.Selector.CurrentItem]
        for Markdowner in self.Markdowns:
            if CurrentWidget == Markdowner["Markdowner"].Document:
                return Markdowner["Markdowner"]
        return False

    def MarkdownDown(self):
        Markdowner = self.GetMarkdown()
        if Markdowner:
            Markdowner.SelectNext()

    def MarkdownUp(self):
        Markdowner = self.GetMarkdown()
        if Markdowner:
            Markdowner.SelectPrevious()

    def on_Comment_notify(self, Widget, Property):
        if Property.name == "name":
            self.Selector.Forward()
        elif Property.name == "style":
            self.Selector.Backward()
        elif Property.name == "label":
            self.Selector.Activate(0)
        elif Property.name == "label-widget":
            self.Selector.Activate(1)

    def on_Box_notify(self, Widget, Property):
        if Property.name == "has-tooltip" or Property.name == "parent":
            return
        Index = Notifies.index(Property.name)
        if 14 < Index:
            self.MarkdownCommands[Index - 15]()
            return
        self.Selector.Activate(*Messages[Index])

    def MarkdownItems(self, Item):
        Item["Markdowner"] = Markdown(
            self.Window,
            self.GetPublication,
            Item["Text"],
            "",
            True,
            self.Settings["EnableMarkdown"],
            self.AddPage,
        )
        Item["Box"].pack_start(Item["Markdowner"].Document, True, True, 0)
        self.ShowHiderTexts.append(Item["Markdowner"].TextBox)
        Item["Markdowner"].Fill()

    def on_ReplyText_draw(self, Widget, Discard=""):
        self.ReplyScrolled.get_vadjustment().set_value(0)

    def on_ReplyText_key_release_event(self, Widget, Discard=""):
        TextBuffer = Widget.get_buffer()
        Start = TextBuffer.get_start_iter()
        End = TextBuffer.get_end_iter()
        Text = TextBuffer.get_text(Start, End, False)
        if (
            self.Settings["PromoteLBRYGTK"]
            and Text != ""
            and not self.Save.get_visible()
        ):
            Text += Promo
        self.Markdowns[1]["Text"] = Text
        self.Markdowns[1]["Markdowner"].Text = self.Markdowns[1]["Text"]
        self.Markdowns[1]["Markdowner"].Fill()
        self.Box.show_all()

    def on_UnHideButton_button_press_event(self, Widget, Discard=""):
        self.HideDislikedBox.set_visible(False)
        self.CommentTextBox.set_visible(True)

    def on_ProfileBox_draw(self, Widget, Cairo):
        self.Thumbnailer.Thumbnail.queue_draw()

    def NoToggleBefore(self, Widget):
        Widget.set_name(str(Widget.get_active()))

    def NoToggleAfter(self, Widget):
        Widget.set_active(Widget.get_name() == "True")
