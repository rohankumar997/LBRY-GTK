################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk


class TextColumn:
    def __init__(self, Text, Width, Lines, Dots=False, Dashes=False):
        self.Text, self.Width, self.Lines = Text, Width, Lines
        self.Column = Gtk.Box.new(Gtk.Orientation.VERTICAL, 0)
        RemainingText = Text
        for Index in range(Lines):
            Label, CurrentText = Gtk.Label.new(""), RemainingText
            while True:
                if (
                    Dots
                    and Index == Lines - 1
                    and len(CurrentText) != len(RemainingText)
                ):
                    Layout = Label.create_pango_layout(CurrentText + "...")
                elif (
                    Dashes
                    and len(RemainingText) > len(CurrentText)
                    and not RemainingText[len(CurrentText)].isspace()
                    and not RemainingText[len(CurrentText) - 1].isspace()
                ):
                    Layout = Label.create_pango_layout(CurrentText + "-")
                else:
                    Layout = Label.create_pango_layout(CurrentText)
                if Layout.get_pixel_size().width <= Width:
                    break
                CurrentText = CurrentText[:-1]
            if (
                Dots
                and Index == Lines - 1
                and len(CurrentText) != len(RemainingText)
            ):
                Label.set_text(CurrentText + "...")
            elif (
                Dashes
                and len(RemainingText) > len(CurrentText)
                and not RemainingText[len(CurrentText)].isspace()
                and not RemainingText[len(CurrentText) - 1].isspace()
            ):
                Label.set_text(CurrentText + "-")
            else:
                Label.set_text(CurrentText)
            self.Column.add(Label)
            RemainingText = RemainingText[len(CurrentText) :]
