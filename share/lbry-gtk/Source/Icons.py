################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GdkPixbuf

from Source import Places

Label, Loader = Gtk.Label.new("Label"), GdkPixbuf.PixbufLoader()
Label.show()
Height = Label.get_preferred_height().minimum_height
with open(Places.ImageDir + "lbry-gtk-lbc.svg", "r") as File:
    Svg = File.read()
Loader.set_size(Height, Height)
Loader.write(Svg.encode())
Loader.close()
LBCLabel = Loader.get_pixbuf()

with open(Places.ImageDir + "lbry-gtk.svg", "r") as File:
    Svg = File.read()
Loader = GdkPixbuf.PixbufLoader()
Loader.write(Svg.encode())
Loader.close()
LogoBig = Loader.get_pixbuf()
