################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import json, gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

from Source import Places


class Channels:
    def __init__(self, Publication, Window, ChannelList="", Settings=""):
        Builder = Gtk.Builder.new_from_file(Places.GladeDir + "Channels.glade")
        Builder.connect_signals(self)
        self.Publication = Publication
        self.Window, self.Channels = Window, Builder.get_object("Channels")
        self.Active = Builder.get_object("Active")
        self.ChannelsBox = Builder.get_object("ChannelsBox")
        if ChannelList != "" and Settings != "":
            self.Update(ChannelList, Settings)

    def StopScroll(self, Widget, Event):
        Widget.emit_stop_by_name("scroll-event")

    def Update(self, ChannelList, Settings):
        if not self.Publication:
            DefaultChannel = Settings["CommentChannel"]
        self.ChannelList, Index, DefaultChannel = ChannelList, 0, ""
        self.Channels.remove_all()

        for Channel in self.ChannelList:
            self.Channels.append(None, Channel[0])
            if Channel[0] == DefaultChannel:
                self.Channels.set_active(Index)
            Index += 1

        if (
            self.Channels.get_active() == -1
            and self.Channels.get_model().iter_n_children() != 0
        ):
            self.Channels.set_active(0)

    def Get(self):
        return self.ChannelList[self.Channels.get_active()]
