################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import threading, json, gi

gi.require_version("Gtk", "3.0")
from gi.repository import GLib, Gtk, Gdk

from flbry import comments

from Source import Places, Popup
from Source.Icons import LBCLabel


class CommentControlTop:
    def __init__(self, *args):
        (
            self.Window,
            self.Row,
            self.GetPublication,
            self.ChannelList,
            self.CommentServer,
            self.Box,
            self.ShowHiderTexts,
            self.Markdowns,
            self.Comment,
            self.ReplyText,
            self.ReplyScrolled,
            self.PreviewBox,
            self.Save,
            self.Post,
            self.on_ReplyText_key_release_event,
            self.Stater,
            self.AddPage,
        ) = args
        Builder = Gtk.Builder.new_from_file(
            Places.GladeDir + "CommentControlTop.glade"
        )
        Builder.connect_signals(self)
        self.CommentID = self.Row[4]
        self.CommentControlTop = Builder.get_object("CommentControlTop")
        self.Channel = Builder.get_object("Channel")
        self.Support = Builder.get_object("Support")
        self.CommentDate = Builder.get_object("CommentDate")
        self.Edit = Builder.get_object("Edit")
        self.Delete = Builder.get_object("Delete")
        self.EditImage = Builder.get_object("EditImage")
        self.UndoImage = Builder.get_object("UndoImage")
        self.LBC1 = Builder.get_object("LBC1")
        self.LBC2 = Builder.get_object("LBC2")
        self.Content = Builder.get_object("Content")
        self.Value = Builder.get_object("Value")
        self.ContentIcon = Builder.get_object("ContentIcon")
        self.LBC1.set_from_pixbuf(LBCLabel)
        self.LBC2.set_from_pixbuf(LBCLabel)

    def on_Channel_button_press_event(self, Widget, Event):
        args = [self.Row[5]]
        if Event.button == Gdk.BUTTON_PRIMARY:
            threading.Thread(None, self.GetPublication, None, args).start()
        elif Event.button == Gdk.BUTTON_MIDDLE:
            self.AddPage(".", "", self.Stater.Export(self.GetPublication, args))

    def on_Edit_button_press_event(self, Widget, Event=""):
        TextBuffer = self.ReplyText.get_buffer()
        ToSet = Widget.get_image() == self.EditImage
        if ToSet:
            TextBuffer.set_text(self.Row[3])
            Widget.set_image(self.UndoImage)
        else:
            TextBuffer.set_text("")
            Widget.set_image(self.EditImage)
        Widget.set_tooltip_text(["Edit", "Undo"][ToSet])
        Visible = self.ReplyScrolled.get_name() == "True"
        self.ReplyScrolled.set_visible(ToSet or Visible)
        self.PreviewBox.get_parent().set_visible(ToSet or Visible)
        self.Save.set_visible(ToSet)
        self.Post.set_visible(not ToSet and Visible)
        self.on_ReplyText_key_release_event(self.ReplyText)

    def on_Delete_button_press_event(self, Widget, Event=""):
        for ChannelItem in self.ChannelList:
            if ChannelItem[3] == self.Row[5]:
                Channel = [ChannelItem[0], ChannelItem[-1]]
                break
        GLib.idle_add(self.DeleteHelper, Widget, Channel)

    def DeleteHelper(self, Widget, Channel):
        Dialog = Gtk.MessageDialog(
            self.Window, buttons=Gtk.ButtonsType.OK_CANCEL
        )
        Dialog.props.text = "Are you sure you want to delete the comment?"
        Response = Dialog.run()
        Dialog.destroy()
        if Response == Gtk.ResponseType.OK:
            args = [Widget, Channel]
            threading.Thread(None, self.DeleteThread, None, args).start()

    def DeleteThread(self, Widget, Channel):
        with open(Places.ConfigDir + "Session.json", "r") as File:
            Session = json.load(File)
        ErrorOrData = comments.delete(
            self.CommentID,
            *Channel,
            self.CommentServer,
            server=Session["Server"]
        )
        if isinstance(ErrorOrData, str):
            Popup.Error(ErrorOrData, self.Window)
            return
        GLib.idle_add(self.DeleteUpdate, Widget)

    def DeleteUpdate(self, Widget):
        Expander = self.Box.get_parent().get_parent()
        RepliesFrame = Expander.get_parent()
        Comment = RepliesFrame.get_parent()
        self.ShowHiderTexts.remove(self.Markdowns[0]["Markdowner"].TextBox)
        self.ShowHiderTexts.remove(self.Markdowns[1]["Markdowner"].TextBox)
        self.Comment.destroy()
        if Expander.get_name() == "RepliesExpander":
            Children = len(self.Box.get_children())
            Expander.set_label("Replies (" + str(Children) + ")")
            if Children == 0:
                RepliesFrame.set_no_show_all(True)
                RepliesFrame.hide()
        Comment.show_all()

    def NoToggleBefore(self, Widget):
        Widget.set_name(str(Widget.get_active()))

    def NoToggleAfter(self, Widget):
        Widget.set_active(Widget.get_name() == "True")
