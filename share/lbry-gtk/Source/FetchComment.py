################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import gi, json, queue, threading, concurrent.futures, time

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GLib

from flbry import comments, settings, channel

from Source import Places, Popup, Settings
from Source.Comment import Comment


class FetchComment:
    def __init__(self, *args):
        (
            self.Window,
            self.ShowHiderTexts,
            self.GetPublication,
            self.Stater,
            self.AddPage,
            self.CommentExpander,
        ) = args
        self.Queues = []

    def SingleComment(self, *Args):
        Commenter = Comment(
            self.ShowHiderTexts,
            *Args,
            self.Window,
            self.SingleComment,
            self.GetPublication,
            self.Stater,
            self.AddPage,
            self.CommentExpander,
        )

    def DisplayPrice(self, Expander, ChannelID, CommentServer):
        CommentSettings = comments.setting_get(ChannelID, CommentServer)
        if isinstance(CommentSettings, str):
            return
        Amount = CommentSettings["min_tip_amount_comment"]
        if Amount != None:
            GLib.idle_add(self.UpdateExpander, Expander, Amount)

    def UpdateExpander(self, Expander, Amount):
        Expander.set_label("Comments (Tip " + str(Amount) + " LBC to comment)")

    def DestroyCommentBox(self):
        if hasattr(self, "CommentBox"):
            self.CommentBox.destroy()

    def KillAll(self):
        GLib.idle_add(self.DestroyCommentBox)
        while len(self.Queues) != 0:
            for Queue in self.Queues:
                Queue.put(True)
            time.sleep(0.01)

    def Fetch(self, *Args):
        self.CommentBox = Args[1]
        self.Threads = concurrent.futures.ThreadPoolExecutor(max_workers=1)
        self.Threads.submit(self.FetchBranch, *Args)

    def FetchBranch(
        self,
        ClaimID,
        Box,
        ExitQueue,
        CommentServer,
        PChannel,
        Session,
        LBRYGTKSettings,
        PChannelName,
        PChannelId,
        CommentID="",
    ):
        self.Queues.append(ExitQueue)
        Channel, PageNumber, PageIndex, RowQueue = ["", ""], 1, 0, queue.Queue()
        ChannelList = channel.channel_list(server=Session["Server"])
        for ChannelItem in ChannelList:
            if ChannelItem[0] == LBRYGTKSettings["CommentChannel"]:
                Channel = [ChannelItem[0], ChannelItem[-1]]
                break

        CommentPerLoading = int(LBRYGTKSettings["CommentPerLoading"])

        while PageIndex != PageNumber:
            try:
                ExitQueue.get(block=False)
                return self.Queues.remove(ExitQueue)
            except:
                pass

            PageIndex += 1

            Value = comments.list(
                ClaimID,
                CommentServer,
                parent_id=CommentID,
                page_size=CommentPerLoading,
                page=PageIndex,
                server=Session["Server"],
                channel_name=PChannelName,
                channel_id=PChannelId,
            )
            if len(Value) != 2:
                return self.Queues.remove(ExitQueue)
            Rows, PageNumber, CommentIDs = *Value, ""
            if isinstance(Rows, str):
                return self.Queues.remove(ExitQueue)

            for Row in Rows:
                CommentIDs += str(Row[4]) + ","

            Reactions = comments.reaction_list(
                *Channel, CommentServer, CommentIDs, server=Session["Server"]
            )

            for Row in Rows:
                try:
                    ExitQueue.get(block=False)
                    return self.Queues.remove(ExitQueue)
                except:
                    pass

                try:
                    OtherReaction = Reactions["others_reactions"][Row[4]]
                    Likes = OtherReaction["like"]
                    Dislikes = OtherReaction["dislike"]
                    CreatorLike = OtherReaction["creator_like"] == 1
                except:
                    Likes, Dislikes, CreatorLike = 0, 0, False

                try:
                    MyReaction = Reactions["my_reactions"][Row[4]]
                    CreatorLike = CreatorLike or MyReaction["creator_like"] == 1
                    Likes += MyReaction["like"]
                    Dislikes += MyReaction["dislike"]
                    Like = MyReaction["like"] == 1
                    Dislike = MyReaction["dislike"] == 1
                except:
                    Like, Dislike = False, False

                GLib.idle_add(
                    self.SingleComment,
                    ClaimID,
                    Box,
                    Row,
                    [Likes, Dislikes, Like, Dislike, CreatorLike],
                    CommentServer,
                    False,
                    False,
                    LBRYGTKSettings,
                    ChannelList,
                    PChannel,
                    RowQueue,
                )

                if Row[0] != 0:
                    NewCommentID = Row[4]
                    try:
                        NewBox = RowQueue.get(timeout=5)
                    except:
                        return self.Queues.remove(ExitQueue)

                    Args = [ClaimID, NewBox, queue.Queue(), CommentServer]
                    Args.extend([PChannel, Session, LBRYGTKSettings])
                    Args.extend([PChannelName, PChannelId, NewCommentID])
                    self.Threads.submit(self.FetchBranch, *Args)
        self.Queues.remove(ExitQueue)
