# TODO:

- Do not use GTK get_name/set_name
- Upload settings
- Delete button
- Documentation
- Comment ordering
- Extra buttons
- Optional repost following
- Native notifications
- Follow sync
- Login improvements (thread, password shower)
- Statistics page
- Cache management
- First result in search is resolve
- Simple upload
- Display time in publications
- Tab history
- Edit/delete own publications
- Comment scrolling from notifications
- Set channel notifications
- Display version number
- Debug window
- Delete notifications
- Organization repo
- Split out Advanced Search
- Split up Startup
- Tooltips everywhere
- Work without internet
- Not publication (settings, advanced search)
- Optional GTK CSD
- Collapsible sidebar
- Titles in publication page
- Scroll list with keybind selection
- Error log to file
- Dependencies in README, docs links in CONTRIBUTING
- Check keybinds on every startup
- Fallback font for emojis
- Ask for download location
- Scroll back markdown after unhiding
- Different placeholder for channels
- Edit text in external editor
- Global downloader class
- Multi modifier keybinds
- Page up/down keybinds
- Organize keybinds
